from contextlib import contextmanager
from typing import Any, Dict, List

from fastapi import APIRouter, Depends, Request
from fastapi.exceptions import HTTPException as FastAPIHTTPException
from pydantic import BaseModel
from starlette.exceptions import HTTPException as StarletteHTTPException
from starlette.responses import JSONResponse, Response

from framework.constants import ResponseMsg, ResponseCode, COMMON_JSON_RESPONSE_KEYS
from framework.db import Database
from framework.exceptions import ValidationError
from framework.models import Context, Environment


class ReadModel(BaseModel):
    model: str
    id: int
    fields: list


class DeleteModel(BaseModel):
    model: str
    ids: list


class CreateModel(BaseModel):
    model: str
    data: dict


class UpdateModel(BaseModel):
    model: str
    data: dict


class ListModel(BaseModel):
    model: str
    fields: list
    current: int = 1
    size: int = 20
    query: dict = None
    order: list = None


class NameSearchModel(BaseModel):
    model: str
    search_key: str = None


class JsonResponse(JSONResponse):
    """ 统一JSON返回格式 """

    def render(self, content: Any) -> bytes:
        if isinstance(content, dict) and content.keys() == COMMON_JSON_RESPONSE_KEYS:
            pass
        else:
            content = self.success(data=content)
        return super().render(content)

    @classmethod
    def success(
            cls,
            code: int = ResponseCode.SUCCESS,
            msg: str = ResponseMsg.SUCCESS,
            data: Any = None
    ) -> dict:
        return {
            'code': code,
            'msg': msg,
            'data': data
        }

    @classmethod
    def error(
            cls,
            code: int = ResponseCode.ERROR,
            msg: str = ResponseMsg.ERROR,
            data: Any = None
    ) -> dict:
        return {
            'code': code,
            'msg': msg,
            'data': data
        }


def get_env(request: Request):
    db = Database().SessionLocal()
    req_context = request.scope.get('context', {})
    ctx = Context(
        tz=req_context.get('tz'),
        language=req_context.get('language'),
        user_info={
            'user_id': req_context.get('user_id'),
            'name': req_context.get('name'),
            'username': req_context.get('username')
        }
    )
    env = Environment(db=db, context=ctx)
    try:
        yield env
    finally:
        db.close()


@contextmanager
def env_scope(env: Environment = Depends(get_env)):
    try:
        yield env
        env.db.commit()
    except Exception as e:
        env.db.rollback()
        raise e


router = APIRouter(prefix='/api/common')


@router.post(
    path='/read',
    response_class=JsonResponse
)
async def read(
        param: ReadModel,
        env=Depends(get_env)
) -> Dict[str, Any]:
    """ 查询记录详细信息 """
    with env_scope(env=env) as scoped_env:
        result = await scoped_env[param.model].read(param.id, param.fields)
        if not result:
            raise ValidationError('api.error.record_not_found')
        return await result.to_dict(param.fields)


@router.post(
    path='/delete',
    response_class=JsonResponse
)
async def delete(
        param: DeleteModel,
        env=Depends(get_env)
):
    """ 批量删除记录 """
    with env_scope(env=env) as scoped_env:
        return await scoped_env[param.model].delete(param.ids)


@router.post(
    path='/update',
    response_class=JsonResponse
)
async def update(
        param: UpdateModel,
        env=Depends(get_env)
):
    """ 更新记录 """
    with env_scope(env=env) as scoped_env:
        record_id = param.data.get('id')
        if not record_id:
            raise ValidationError('param [data.id] is required')
        return await scoped_env[param.model].set('id', record_id).update(param.data)


@router.post(
    path='/create',
    response_class=JsonResponse
)
async def create(
        param: CreateModel,
        env=Depends(get_env)
) -> Dict[str, Any]:
    """ 创建记录 """
    with env_scope(env=env) as scoped_env:
        record = await scoped_env[param.model].create(param.data)
        return {'id': record.id}


@router.post(
    path='/list',
    response_class=JsonResponse
)
async def search_read(
        param: ListModel,
        env=Depends(get_env)
) -> Dict[str, Any]:
    """ 分页查询 """
    with env_scope(env=env) as scoped_env:
        limit = param.size or 20
        model_obj = scoped_env[param.model]
        total = await model_obj.search_count(param.query)
        rows = await model_obj.search_read(
            fields=param.fields,
            query=param.query,
            offset=(param.current - 1) * limit,
            limit=limit,
            order=param.order
        )
        return {
            'total': total,
            'result': [await row.to_dict(param.fields) for row in rows]
        }


@router.post(
    path='/name_search',
    response_class=JsonResponse
)
async def name_search(
        param: NameSearchModel,
        env=Depends(get_env)
) -> List[Dict[str, Any]]:
    """ 下拉列表查询 """
    with env_scope(env=env) as scoped_env:
        return await scoped_env[param.model].name_search(param.search_key)


async def http_exception_handler(_: Request, e: Exception) -> Response:
    """ HTTP异常处理 """
    if isinstance(e, (FastAPIHTTPException, StarletteHTTPException)):
        return JsonResponse(
            status_code=e.status_code,
            content=JsonResponse.error(
                code=ResponseCode.INTERNAL_SERVER_ERROR,
                msg=ResponseMsg.INTERNAL_SERVER_ERROR,
                data=e.detail
            )
        )
    elif isinstance(e, ValidationError):
        return JsonResponse(
            content=JsonResponse.error(
                code=e.code,
                msg=e.msg,
                data=e.data
            )
        )
    else:
        return JsonResponse(
            status_code=500,
            content=JsonResponse.error(
                code=ResponseCode.INTERNAL_SERVER_ERROR,
                msg=ResponseMsg.INTERNAL_SERVER_ERROR,
                data=str(e)
            )
        )
