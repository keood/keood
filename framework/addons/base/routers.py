from fastapi import Depends, APIRouter
from pydantic import BaseModel

from framework.models import Environment, ModelContainer
from framework.routes import get_env, JsonResponse


class LoginModel(BaseModel):
    username: str
    password: str


class GetViewsModel(BaseModel):
    model: str
    fields: list


router = APIRouter(prefix='/api/base')


@router.post("/login", response_class=JsonResponse)
async def login(params: LoginModel, env=Depends(get_env)):
    return await env['user'].login(params.username, params.password)


@router.get("/current_user", response_class=JsonResponse)
async def current_user(env: Environment = Depends(get_env)):
    return await env['user'].current_user()


@router.get("/menus", response_class=JsonResponse)
async def get_user_menus(env: Environment = Depends(get_env)):
    return await env['menu'].get_user_menus()


@router.post("/get_view_info", response_class=JsonResponse)
async def get_view_info(params: GetViewsModel, env: Environment = Depends(get_env)):
    model = ModelContainer.get(params.model)
    return await model.get_view_info(env, params.fields)
