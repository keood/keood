from sqlalchemy import Column

from framework.fields import String, Many2one, Boolean, One2many
from framework.models import Model


class Role(Model):
    __tablename__ = 't_role'
    __name = 'role'

    name = Column(String, unique=True, index=True)
    model_rule_ids = Column(One2many(model='role.model.rule', rel_field_name='role_id'))


class RoleModelRule(Model):
    __tablename__ = 't_role_model_rule'
    __name = 'role.model.rule'

    role_id = Column(Many2one(model='role', ondelete='cascade'), index=True, nullable=False)
    model = Column(String)
    is_create = Column(Boolean, default=False)
    is_update = Column(Boolean, default=False)
    is_read = Column(Boolean, default=False)
    is_delete = Column(Boolean, default=False)
