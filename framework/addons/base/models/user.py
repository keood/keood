from sqlalchemy import Column, Boolean, Enum

from framework.addons.base.comm.constant import UserConstant
from framework.config import Config
from framework.constants import Timezone, Language
from framework.exceptions import ValidationError
from framework.fields import Many2many, String
from framework.models import Model, orm, ORMAction
from framework.utils.crypto_util import HashCrypt
from framework.utils.jwt_util import JWT


class User(Model):
    __tablename__ = 't_user'
    __name = 'user'

    username = Column(
        String,
        unique=True,
        index=True,
        info={
            'string': {
                Language.EN_US.value: 'Username',
                Language.ZH_CN.value: '用户名'
            }
        })
    password = Column(
        String)
    name = Column(
        String,
        unique=True,
        index=True,
        info={
            'string': {
                Language.EN_US.value: 'Name',
                Language.ZH_CN.value: '姓名'
            }
        })
    tz = Column(
        Enum(Timezone),
        default=Timezone.AISA_SHANGHAI,
        info={
            'string': {
                Language.EN_US.value: 'Timezone',
                Language.ZH_CN.value: '时区'
            }
        })
    language = Column(
        Enum(Language),
        default=Language.ZH_CN,
        info={
            'string': {
                Language.EN_US.value: 'Language',
                Language.ZH_CN.value: '语言'
            },
            'selection': {
                Language.EN_US.value: {
                    Language.EN_US.value: 'English',
                    Language.ZH_CN.value: '英语'
                },
                Language.ZH_CN.value: {
                    Language.EN_US.value: 'Chinese',
                    Language.ZH_CN.value: '汉语'
                }
            }
        })
    is_active = Column(
        Boolean,
        default=True,
        info={
            'string': {
                Language.EN_US.value: 'Active',
                Language.ZH_CN.value: '有效'
            }
        })

    role_ids = Column(
        Many2many(model='role', table_name='rel_user_role', field_name='user_id', rel_field_name='role_id'),
        info={
            'string': {
                Language.EN_US.value: 'Role',
                Language.ZH_CN.value: '角色'
            }
        }
    )

    @orm(action=ORMAction.create)
    async def create(self, values):
        password = values.get("password")
        if not password:
            password = Config().get_string('system.pwd_default')
        encrypt_password = HashCrypt.sha256_hash(password, Config().get_string('system.pwd_salt'))
        values['password'] = encrypt_password
        return await super().create(values)

    async def to_dict(self, fields: list) -> dict:
        res = await super().to_dict(fields)
        res.pop('password', None)
        return res

    async def login(self, username: str, password: str):
        user = await self.search_read(
            ['id', 'name', 'username', 'password', 'tz', 'language', 'is_active'],
            {'rel': 'and', 'conditions': [{'condition': ('username', '=', username)}]},
            limit=1
        )
        if not user or not user.is_active:
            raise ValidationError(UserConstant.USER_NOT_EXIST_ERROR)
        encrypt_password = HashCrypt.sha256_hash(password, Config().get_string('system.pwd_salt'))
        if user.password != encrypt_password:
            raise ValidationError(UserConstant.PWD_ERROR_ERROR)
        data = {
            'user_id': user.id,
            'name': user.name,
            'username': username,
            'tz': user.tz.value,
            'language': user.language.value
        }
        JWT.generate_token(data)
        return data

    async def current_user(self) -> dict:
        user = await self.read(
            self.env.context.user_info['user_id'],
            ['id', 'name', 'username', 'tz', 'language', 'is_active']
        )
        if not user or not user.is_active:
            raise ValidationError(UserConstant.USER_NOT_EXIST_ERROR)
        return {
            'user_id': user.id,
            'name': user.name,
            'username': user.username,
            'tz': user.tz.value,
            'language': user.language.value
        }
