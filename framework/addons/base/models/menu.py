from sqlalchemy import Column, Integer

from framework.addons.base.comm.constant import UserConstant
from framework.constants import Language
from framework.exceptions import ValidationError
from framework.fields import String
from framework.models import Model, Many2one


class Menu(Model):
    __tablename__ = 't_menu'
    __name = 'menu'

    name = Column(
        String(translate=True),
        info={
            'string': {
                Language.EN_US.value: 'Name',
                Language.ZH_CN.value: '名称'
            }
        }
    )
    icon = Column(
        String,
        info={
            'string': {
                Language.EN_US.value: 'Icon',
                Language.ZH_CN.value: '图标'
            }
        }
    )
    path = Column(
        String,
        unique=True,
        info={
            'string': {
                Language.EN_US.value: 'Path',
                Language.ZH_CN.value: '路径'
            }
        }
    )
    parent_id = Column(
        Many2one(model='menu'),
        info={
            'string': {
                Language.EN_US.value: 'Parent',
                Language.ZH_CN.value: '父菜单'
            }
        }
    )
    sequence = Column(
        Integer,
        info={
            'string': {
                Language.EN_US.value: 'Sequence',
                Language.ZH_CN.value: '序号'
            }
        }
    )

    async def get_user_menus(self):
        user = await self.env['user'].read(
            self.env.context.user_info['user_id'],
            ['id', 'is_active']
        )
        if not user or not user.is_active:
            raise ValidationError(UserConstant.USER_NOT_EXIST_ERROR)
        menus = await self.search_read(
            ['id', 'parent_id', 'name', 'path', 'icon'],
            {},
            order=[('parent_id', 'asc'), ('sequence', 'asc')],
            no_page=True
        )
        top_menus = [x for x in menus if not x.parent_id]
        parent_ids = set([x.parent_id[0] for x in menus if x.parent_id])
        result = []
        self._recursive_read_menus(top_menus, result, menus, parent_ids)
        return result

    @classmethod
    def _recursive_read_menus(cls, menus: list, result: list, all_menus: list, parent_ids: set):
        for menu in menus:
            child_menus = [x for x in all_menus if x.parent_id and x.parent_id[0] == menu.id]
            child_result = []
            cls._recursive_read_menus(child_menus, child_result, all_menus, parent_ids)
            result.append({
                'name': menu.name,
                'path': menu.path,
                'icon': menu.icon,
                'routes': child_result
            })
            if menu.id not in parent_ids:
                result.append({
                    'name': menu.name,
                    'path': menu.path + '/create',
                    'hideInMenu': True
                })
                result.append({
                    'name': menu.name,
                    'path': menu.path + '/:id',
                    'hideInMenu': True
                })
