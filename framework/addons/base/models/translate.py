from sqlalchemy import Column, Integer, JSON

from framework.constants import Language
from framework.fields import String
from framework.models import Model


class Translate(Model):
    __tablename__ = 't_translate'
    __name = 'translate'

    model = Column(
        String,
        info={
            'string': {
                Language.EN_US.value: 'Model',
                Language.ZH_CN.value: '模型'
            }
        }
    )
    record_id = Column(
        Integer,
        info={
            'string': {
                Language.EN_US.value: 'Record ID',
                Language.ZH_CN.value: '记录ID'
            }
        }
    )
    field = Column(
        String,
        info={
            'string': {
                Language.EN_US.value: 'Field',
                Language.ZH_CN.value: '字段名'
            }
        }
    )
    translate = Column(
        JSON,
        info={
            'string': {
                Language.EN_US.value: 'Translate',
                Language.ZH_CN.value: '翻译'
            }
        }
    )
