from enum import Enum

from sqlalchemy import and_, or_


class ResponseCode:
    SUCCESS = 0
    ERROR = 1
    UNAUTHORIZED = 401
    INTERNAL_SERVER_ERROR = 500


class ResponseMsg:
    SUCCESS = "api.success"
    ERROR = "api.error"
    TOKEN_INVALID_ERROR = "api.error.token.invalid"
    INTERNAL_SERVER_ERROR = 'api.error.internal_server_error'


class Datetime:
    FORMAT = "%Y-%m-%d %H:%M:%S"


class System:
    SUPERUSER_ID = 1
    SUPERUSER_USERNAME = 'admin'
    SUPERUSER_NAME = 'Admin'


class Timezone(Enum):
    UTC = 'UTC'
    AISA_SHANGHAI = 'Asia/Shanghai'


class Language(Enum):
    EN_US = 'en-US'
    ZH_CN = 'zh-CN'


class RelationFieldType(Enum):
    """ 关联字段类型 """
    MANY2ONE = 'Many2one'
    STRING_TRANSLATE = 'StringTranslate'


class CompareSymbol(Enum):
    """ 比较符号 """
    LIKE = 'like'
    ILIKE = 'ilike'
    IN = 'in'
    EQUAL = '='
    NOT_EQUAL = '!='
    GT = '>'
    LT = '<'
    GTE = '>='
    LTE = '<='


TZ_OFFSET_MAP = {
    Timezone.UTC.value: 0,
    Timezone.AISA_SHANGHAI.value: 480
}

QUERY_REL_FUNC_MAP = {
    'and': and_,
    'or': or_
}

COMMON_JSON_RESPONSE_KEYS = {'code', 'msg', 'data'}
