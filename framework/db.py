import logging

from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError, StatementError
from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy.orm.query import Query as _Query
from time import sleep

from .config import Config
from .decorators import singleton

_logger = logging.getLogger(__name__)

Base = declarative_base()


class RetryingQuery(_Query):
    __max_retry_count__ = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __iter__(self):
        attempts = 0
        while True:
            attempts += 1
            try:
                return super().__iter__()
            except OperationalError as ex:
                if "server closed the connection unexpectedly" not in str(ex):
                    raise
                if attempts <= self.__max_retry_count__:
                    sleep_for = 2 ** (attempts - 1)
                    logging.error(
                        "Database connection error: retrying Strategy => sleeping for {}s"
                        " and will retry (attempt #{} of {}) \n Detailed query impacted: {}".format(
                            sleep_for, attempts, self.__max_retry_count__, ex)
                    )
                    sleep(sleep_for)
                    continue
                else:
                    raise
            except StatementError as ex:
                if "reconnect until invalid transaction is rolled back" not in str(ex):
                    raise
                self.session.rollback()


@singleton
class Database:
    def __init__(self):
        config = Config()
        self.host = config.get_string('datasource.host')
        self.port = config.get_int('datasource.port')
        self.name = config.get_string('datasource.name')
        self.user = config.get_string('datasource.user')
        password = config.get_string('datasource.password')
        self.echo = config.get_bool('datasource.echo')
        self.pool_size = config.get_int('datasource.pool_size')
        self.max_overflow = config.get_int('datasource.max_overflow')
        self.pool_recycle = config.get_int('datasource.pool_recycle')
        self.dsn = F'postgresql://{self.user}:{password}@{self.host}:{self.port}/{self.name}'
        self.engine = create_engine(
            self.dsn,
            echo=self.echo,
            pool_size=self.pool_size,
            max_overflow=self.max_overflow,
            pool_recycle=self.pool_recycle,
            pool_pre_ping=True,
            pool_use_lifo=True
        )
        self.SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=self.engine, query_cls=RetryingQuery)
