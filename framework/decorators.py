import time
import threading
from functools import wraps


def synchronized(func):
    """锁装饰器"""
    func.__lock__ = threading.Lock()

    @wraps(func)
    def lock_func(*args, **kwargs):
        with func.__lock__:
            return func(*args, **kwargs)

    return lock_func


def singleton(cls_):
    """单例类装饰器"""

    class wrap_cls(cls_):
        __instance = None

        @synchronized
        def __new__(cls, *args, **kwargs):
            if cls.__instance is None:
                cls.__instance = super().__new__(cls, *args, **kwargs)
                cls.__instance.__init = False
            return cls.__instance

        @synchronized
        def __init__(self, *args, **kwargs):
            if self.__init:
                return
            super().__init__(*args, **kwargs)
            self.__init = True

    wrap_cls.__name__ = cls_.__name__
    wrap_cls.__doc__ = cls_.__doc__
    wrap_cls.__qualname__ = cls_.__qualname__
    return wrap_cls


def func_retry(times=3, sleep=5):
    """
    用于方法运行失败时进行重试的装饰器，判定失败的方式为方法返回的第一个结果的bool值为False
    :param times: 重试次数
    :param sleep: 每次重试间隔的时间，单位秒
    :return:
    """
    def decorator(func):
        @wraps(func)
        def wrapped_function(*args, **kwargs):
            result = func(*args, **kwargs)
            no = times
            while no > 0 and not result[0]:
                time.sleep(sleep)
                result = func(*args, **kwargs)
                no = no - 1
            return result
        return wrapped_function
    return decorator


def async_func_retry(times=3, sleep=5):
    """
    异步
    用于方法运行失败时进行重试的装饰器，判定失败的方式为方法返回的第一个结果的bool值为False
    :param times: 重试次数
    :param sleep: 每次重试间隔的时间，单位秒
    :return:
    """
    def decorator(func):
        @wraps(func)
        async def wrapped_function(*args, **kwargs):
            result = await func(*args, **kwargs)
            no = times
            while no > 0 and not result[0]:
                time.sleep(sleep)
                result = await func(*args, **kwargs)
                no = no - 1
            return result
        return wrapped_function
    return decorator
