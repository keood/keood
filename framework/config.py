import os

import yaml

from .utils import str_util
from .decorators import singleton


def _init_config() -> dict:
    """初始化配置"""
    with open(F'config/config.yml', 'r', encoding='utf-8') as f:
        content = f.read()
        return yaml.safe_load(content)


@singleton
class Config:
    def __init__(self):
        self.data = _init_config()

    def get_string(self, key: str):
        res = os.getenv(str_util.split_point_to_upper_underline(key))
        if not res:
            return str(self._get(key))
        return str(res)

    def get_int(self, key: str):
        res = os.getenv(str_util.split_point_to_upper_underline(key))
        if not res:
            return int(self._get(key))
        return int(res)

    def get_bool(self, key: str) -> bool:
        res = os.getenv(str_util.split_point_to_upper_underline(key))
        if not res:
            return self._get(key)
        return str_util.str_to_bool(res)

    def get(self, key: str):
        res = os.getenv(str_util.split_point_to_upper_underline(key))
        return res or self._get(key)

    def _get(self, key: str):
        res = self.data
        for k in key.split('.'):
            res = res.get(k)
        return res
