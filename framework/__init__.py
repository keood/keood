import json
import logging

from sqlalchemy.orm import Session

from framework.config import Config
from framework.constants import System, Language, Timezone
from framework.db import Database, Base
from framework.models import Environment, Context, ModelContainer
from framework.utils.crypto_util import HashCrypt

_logger = logging.getLogger('root')


def load_models():
    """
    加载model
    """
    _logger.info('loading models start')
    from . import addons as framework_addons
    import addons as extra_addons
    _logger.debug('models path: [%s, %s]', framework_addons, extra_addons)
    for mapper in Base.registry.mappers:
        ModelContainer.register(mapper.class_.get_model_name(), mapper.class_)
    _logger.info('loading models completed')


async def create_super_user():
    """
    创建超级用户
    """
    with Database().SessionLocal() as session:
        env = Environment(session, Context(Timezone.AISA_SHANGHAI.value, Language.ZH_CN.value, {}))
        user_obj = env['user']
        user = await user_obj.read(System.SUPERUSER_ID, ['id'])
        if not user:
            conf = Config()
            encrypt_password = HashCrypt.sha256_hash(
                conf.get_string('system.pwd_default'),
                conf.get_string('system.pwd_salt')
            )
            user = user_obj.__class__(
                id=System.SUPERUSER_ID,
                username=System.SUPERUSER_USERNAME,
                password=encrypt_password,
                name=System.SUPERUSER_NAME,
                created_by=System.SUPERUSER_ID,
                updated_by=System.SUPERUSER_ID
            )
            session.add(user)
            session.commit()


async def init_menus():
    """
    初始化菜单
    """
    with (Database().SessionLocal() as session):
        env = Environment(session, Context(Timezone.AISA_SHANGHAI.value, Language.ZH_CN.value, {}))
        menu_obj = env['menu']
        with open('web/config/routes.ts', 'r') as f:
            content = f.read()
            content = content.replace('export default JSON.parse(`', '').replace('`);', '')
            menus = json.loads(content)
        db_menus = await menu_obj.search_read(['id', 'path', 'parent_id'], {}, no_page=True)
        db_menus_map = {x.path: x for x in db_menus}
        await _recursive_deal_menus(menu_obj, session, menus, db_menus_map)
        if db_menus_map:
            await menu_obj.delete([v.id for v in db_menus_map.values()])
        session.commit()


async def _recursive_deal_menus(menu_obj, session: Session, menus: list, db_menus_map: dict, parent_menu_instance=None):
    """
    递归处理菜单配置
    """
    for menu in menus:
        if 'layout' in menu and not menu['layout']:
            # 不显示的菜单不处理
            continue
        if menu['path'] == '/':
            # 根目录不处理
            continue
        if 'hideInMenu' in menu and menu['hideInMenu']:
            # 隐藏菜单不处理
            continue
        if menu['path'] in db_menus_map:
            menu_instance = db_menus_map[menu['path']]
            db_menus_map.pop(menu['path'])
        else:
            menu_instance = menu_obj.__class__(
                name=menu['name'],
                path=menu['path'],
                icon=menu.get('icon'),
                sequence=menu.get('sequence', 10)
            )
            if parent_menu_instance:
                menu_instance.parent_id = parent_menu_instance.id
            session.add(menu_instance)
            session.flush()
        child_menus = menu.get('routes')
        if child_menus:
            await _recursive_deal_menus(menu_obj, session, child_menus, db_menus_map, menu_instance)
