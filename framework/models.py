import abc
import datetime
from enum import Enum
from functools import wraps
from itertools import groupby
from typing import List, Optional, TypeVar, Tuple, Any, Dict

from sqlalchemy import (
    Column,
    update,
    delete,
    select,
    alias,
    text,
    Select,
    desc,
    asc,
    TextClause,
    and_,
    ColumnElement, insert
)
from sqlalchemy.orm import Session
from sqlalchemy.sql.selectable import NamedFromClause

from framework.constants import TZ_OFFSET_MAP, QUERY_REL_FUNC_MAP, Language, RelationFieldType, CompareSymbol
from framework.db import Base
from framework.exceptions import ModelNotFoundException, ValidationError
from framework.fields import Many2one, String, Many2many, Integer, DateTime, One2many

ModelClass = TypeVar('ModelClass', bound='Model')


class Context:
    """ 上下文，每个接口和每次ORM操作都应有上下文 """

    def __init__(self, tz: str, language: str, user_info: dict):
        self.tz_name = tz
        self.tz = datetime.timezone(
            datetime.timedelta(
                minutes=(TZ_OFFSET_MAP.get(tz) or 0)
            )
        )
        self.language = language
        self.user_info = user_info


class Environment:
    """ 环境，每个接口和ORM的运行环境，包括数据库session和上下文 """

    def __init__(self, db: Session, context: Context):
        self.db: Session = db
        self.context = context

    def __getitem__(self, item) -> ModelClass:
        """
        根据item字符串获取新的model类的实例，并把env传递给新的实例
        用于切换所需操作的模型对象
        :param item: model名称
        :return:
        """
        model_obj = ModelContainer.get_obj(item)
        model_obj.env = self
        return model_obj


class ORMAction(Enum):
    create = 'create'
    update = 'update'
    read = 'read'
    delete = 'delete'


class BaseHandler(metaclass=abc.ABCMeta):
    def __init__(self, args):
        self.args = args
        self.instance = None
        self.model = None

    async def handle(self):
        self.instance = self.args[0]
        self.model = self.instance.get_model_name()
        self._check_permission()
        self._deal_args()

    @abc.abstractmethod
    def _check_permission(self):
        raise NotImplementedError('method must be implemented')

    @abc.abstractmethod
    def _deal_args(self):
        raise NotImplementedError('method must be implemented')


class CreateHandler(BaseHandler):

    def _check_permission(self):
        pass

    def _deal_args(self):
        values: dict = self.args[1]
        user_info: dict = self.instance.env.context.user_info
        user_id = user_info['user_id']
        values.update({'created_by': user_id, 'updated_by': user_id})


class UpdateHandler(BaseHandler):

    def _check_permission(self):
        pass

    def _deal_args(self):
        values: dict = self.args[1]
        user_info: dict = self.instance.env.context.user_info
        values['updated_by'] = user_info['user_id']


class ReadHandler(BaseHandler):

    def _check_permission(self):
        pass

    def _deal_args(self):
        pass


class DeleteHandler(BaseHandler):

    def _check_permission(self):
        pass

    def _deal_args(self):
        pass


async def _orm_handler(action: ORMAction, args):
    handler_cls = {
        ORMAction.create: CreateHandler,
        ORMAction.update: UpdateHandler,
        ORMAction.read: ReadHandler,
        ORMAction.delete: DeleteHandler,
    }.get(action)
    handler = handler_cls(args)
    await handler.handle()


def orm(action: ORMAction):
    def decorator(func):
        @wraps(func)
        async def wrapped_function(*args, **kwargs):
            await _orm_handler(action, args)
            return await func(*args, **kwargs)

        return wrapped_function

    return decorator


class Model(Base):
    """
    模型Mixin：定义基础字段，实现基础方法
    """

    __abstract__ = True
    __name: str

    id = Column(
        Integer,
        primary_key=True)
    created_at = Column(
        DateTime,
        default=lambda: datetime.datetime.now(tz=datetime.timezone.utc),
        info={
            'string': {
                Language.EN_US.value: 'Created At',
                Language.ZH_CN.value: '创建时间'
            }
        })
    updated_at = Column(
        DateTime,
        default=lambda: datetime.datetime.now(tz=datetime.timezone.utc),
        onupdate=lambda: datetime.datetime.now(tz=datetime.timezone.utc),
        info={
            'string': {
                Language.EN_US.value: 'Updated At',
                Language.ZH_CN.value: '更新时间'
            }
        })
    created_by = Column(
        Many2one(model='user'),
        info={
            'string': {
                Language.EN_US.value: 'Created By',
                Language.ZH_CN.value: '创建者'
            }
        })
    updated_by = Column(
        Many2one(model='user'),
        info={
            'string': {
                Language.EN_US.value: 'Updated By',
                Language.ZH_CN.value: '更新者'
            }
        })

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__env = None

    @property
    def env(self) -> Environment:
        """ model执行ORM操作时的运行环境 """
        return self.__env

    @env.setter
    def env(self, new_env: Environment):
        """ model执行ORM操作时的运行环境赋值，一般无需调用 """
        if not isinstance(new_env, Environment):
            raise ValueError("env must be type framework.bean.Environment")
        self.__env = new_env

    @classmethod
    def get_model_name(cls):
        return getattr(cls, f'_{cls.__name__}__name')

    def set(self, field, value):
        """
        给字段设置值，可链式调用
        :param field: 字段
        :param value: 值
        """
        setattr(self, field, value)
        return self

    def with_ctx(self, **kwargs):
        """
        往上下文信息中添加新的信息
        :param kwargs:
        :return:
        """
        for k, v in kwargs.items():
            setattr(self.__env.context, k, v)
        return self

    @orm(action=ORMAction.read)
    async def read(self, id, fields: list[str | dict]) -> Optional[ModelClass]:
        """
        根据主键获取数据指定字段列表的值
        :param id: 主键的值
        :param fields: 字段列表
        :return: dict
        """
        if not fields:
            raise ValidationError('when call read method, fields param is required!')
        return await SearchHelper(model=self, fields=fields).read(id)

    @orm(action=ORMAction.delete)
    async def delete(self, ids: list = None):
        await WriteHelper(model=self).delete_help(ids)

    @orm(action=ORMAction.update)
    async def update(self, values: dict):
        await WriteHelper(model=self, values=values).update_help()

    @orm(action=ORMAction.create)
    async def create(self, values: dict):
        await WriteHelper(model=self, values=values).create_help()
        return self

    @orm(action=ORMAction.read)
    async def search_read(
            self,
            fields: List[str | dict],
            query: Dict[str, Any],
            offset=0,
            limit=20,
            order=None,
            no_page=False
    ) -> List | ModelClass:
        """
        批量查询数据方法
        :param fields: 所需要查询出的字段的列表
        :param query: 查询条件，dict, dict类似于
                ```json
                {
                    "rel": "and",
                    "conditions": [
                        {
                            "condition": ["name", "like", "张三"]
                        },
                        {
                            "condition": ["age", ">", 18]
                        }
                     ]
                 }
                ```
        :param offset: offset
        :param limit: limit
        :param order: 排序，例如：[["name", "desc"], ["age", "asc"]]
        :param no_page: 为True时不分页，查询出满足条件的所有数据
        """
        if not fields:
            raise ValidationError('when call search_read method, fields param is required!')
        helper = SearchHelper(model=self, fields=fields, query=query)
        rows = await helper.search_read(order, offset, limit, no_page)
        if rows and offset == 0 and limit == 1:
            return rows[0]
        return rows

    @orm(action=ORMAction.read)
    async def search_count(
            self,
            query: dict
    ) -> int:
        """ 计数方法 """
        return await SearchHelper(model=self, query=query).search_count()

    @orm(action=ORMAction.read)
    async def name_search(
            self,
            search_key: str
    ) -> List[Dict[str, Any]]:
        """ 下拉列表查询方法 """
        return await SearchHelper(model=self, fields=['id', 'name']).name_search(search_key)

    async def to_dict(
            self,
            fields: List[str]
    ) -> Dict[str, Any]:
        """
        返回对象指定字段列表的值
        :param fields: 字段列表
        :return: dict
        """
        fields = [list(f.keys())[0] if isinstance(f, dict) else f for f in fields]
        if not fields:
            raise ValidationError('when call to_dict method, fields param is required!')
        return {f: self._deal_instance_to_dict_field(f) for f in fields}

    @classmethod
    async def get_view_info(
            cls,
            env: Environment,
            fields: list
    ) -> dict:
        """根据字段名称列表获取字段详细信息列表"""
        language = env.context.language
        result_fields = []
        for field in fields:
            m_field = getattr(cls, field)
            rec = {
                'name': field,
                'string': m_field.info.get('string', {}).get(language) or field,
                'type': m_field.type.__class__.__name__
            }
            if rec['type'] == 'String':
                if m_field.type.translate:
                    rec['translate'] = True
            elif rec['type'] == 'Enum':
                rec['selection'] = {
                    e.name: m_field.info.get('selection', {}).get(e.value, {}).get(language) or e.value
                    for e in m_field.type.enum_class
                }
            elif rec['type'] in 'Many2one':
                rec['rel_model'] = m_field.type.model
            elif rec['type'] in 'Many2many':
                rec['rel_model'] = m_field.type.model
            result_fields.append(rec)
        return {'fields': result_fields}

    def _deal_instance_to_dict_field(self, field: str):
        """ 实例处理成dict时，字段处理逻辑 """
        model_field = getattr(self, field)
        if isinstance(model_field, Enum):
            return model_field.name
        return model_field


class ModelContainer:
    """ model集合 """
    __data = {}

    @classmethod
    def register(cls, name: str, model):
        """ 注册model """
        cls.__data[name] = model

    @classmethod
    def get(cls, name: str) -> ModelClass:
        """ 获取ModelClass """
        model = cls.__data.get(name)
        if not model:
            raise ModelNotFoundException('the model {} does not exist'.format(name))
        return model

    @classmethod
    def get_obj(cls, name: str) -> ModelClass:
        """ 获取ModelClass实例化对象 """
        model = cls.__data.get(name)
        if not model:
            raise ModelNotFoundException('the model {} does not exist'.format(name))
        return model()

    @classmethod
    def get_all(cls) -> List[ModelClass]:
        return list(cls.__data.values())


class SelectRelationUnit:
    """ Model查询方法select子句需要的关联信息单元 """

    def __init__(
            self,
            key: str,
            field: Column,
            field_text: TextClause,
            from_alias: List[NamedFromClause],
            join_on: List[ColumnElement[bool]],
            rel_type: RelationFieldType
    ):
        """
        :param key: 唯一标识，用于辨别关联表时是否已关联，避免重复关联
        :param field: Model字段
        :param field_text: select子句中的查询text
        :param from_alias: select子句中的关联表的集合
        :param join_on: select子句关联表的关联条件集合，应与关联表集合按顺序一一对应
        :param rel_type: 关联类型 RelationFieldType
        """
        self.key = key
        self.field = field
        self.text = field_text
        self.from_alias = from_alias
        self.join_on = join_on
        self.rel_type = rel_type


class WhereRelationUnit:
    """ Model查询方法where子句需要的关联信息单元 """

    def __init__(
            self,
            key: str,
            field: Column,
            query_clause: ColumnElement[bool] | TextClause,
            from_alias: List[NamedFromClause],
            join_on: List[ColumnElement[bool]],
    ):
        """
        :param key: 唯一标识，用于辨别关联表时是否已关联，避免重复关联
        :param field: Model字段
        :param query_clause: where子句中的条件表达式
        :param from_alias: where子句所需的join表信息
        :param join_on: where子句所需的join表的关联条件，应与where子句所需的join表信息按顺序一一对应
        """
        self.key = key
        self.field = field
        self.query_clause = query_clause
        self.from_alias = from_alias
        self.join_on = join_on


class SearchHelper:
    """ 批量数据查询辅助类 """

    def __init__(
            self,
            model: ModelClass,
            fields: List[str | dict] = None,
            query: dict = None,
    ):
        self.model = model
        self.field_name_list, self.field_dict = self.__get_field_name_list_and_dict(fields or [])
        self.query = query or {}
        self.field_type_dict = self.__get_field_type_dict()
        self.query_field_name_list = self.__get_query_field_name_list()
        self.all_fields = self.model.__class__.__table__.columns

        self.alias_name_list = []
        self.select_units: List[SelectRelationUnit] = []
        self.select_unit_map: Dict[str, SelectRelationUnit] = {}
        self.where_units: List[WhereRelationUnit] = []
        self.query_clauses: List[ColumnElement[bool]] = []
        self.params: Dict[str, Any] = {}

        self._parse_select()
        self._parse_where()

    async def search_read(
            self,
            order: List[List[str]] | List[Tuple[str, str]] = None,
            offset: int = 0,
            limit: int = 20,
            no_page: bool = False
    ) -> List[ModelClass]:
        """ 分页查询 """
        # select子句
        stmt = select(
            *[getattr(self.model.__class__, x) for x in self.query_field_name_list],
            *[x.text for x in self.select_units]
        )
        # from子句
        stmt = self._set_stmt_join(stmt)
        # where子句
        stmt = stmt.filter(*self.query_clauses)
        # 处理排序
        stmt = self._deal_search_read_order(stmt, order)
        # 处理分页
        stmt = self._deal_search_read_page(stmt, offset, limit, no_page)
        # 执行sql，获取查询结果
        rows = self.model.env.db.execute(stmt, params=self.params).all() or []
        if not rows:
            return rows
        # 处理成实例列表并返回
        records = [self._deal_read_result_to_instance(row) for row in rows]
        # 处理对多字段
        await self._deal_to_many_field(records)
        return records

    async def search_count(self) -> int:
        """ 计数查询 """
        # select子句
        stmt = select(text(f'count(*)')).select_from(self.model.__class__)
        # from子句
        stmt = self._set_stmt_join(stmt, no_select=True)
        # where子句
        stmt = stmt.filter(*self.query_clauses)
        # 执行sql，获取查询结果
        row = self.model.env.db.execute(stmt, params=self.params).first()
        return row[0]

    async def name_search(
            self,
            search_key: str
    ) -> List[Dict[str, Any]]:
        """ 下拉列表查询方法 """
        # select子句
        if self.select_units:
            stmt = select(self.model.__class__.id, self.select_units[0].text)
            # from子句
            stmt = self._set_stmt_join(stmt)
        else:
            stmt = select(self.model.__class__.id, self.model.__class__.name)
        # where子句
        if search_key:
            if self.select_units:
                stmt = stmt.filter(text(f"{str(self.select_units[0].text)} LIKE :search_key"))
            else:
                stmt = stmt.filter(self.model.__class__.name.like(f'%{search_key}%'))
        # 执行sql，获取查询结果
        rows = self.model.env.db.execute(stmt.limit(20), params={'search_key': f'%{search_key}%'}).all() or []
        return [{'label': row[1], 'value': row[0]} for row in rows]

    async def read(
            self,
            id
    ) -> Optional[ModelClass]:
        """
        根据主键获取数据指定字段列表的值
        :param id: 主键的值
        """
        # select子句
        stmt = select(
            *[getattr(self.model.__class__, x) for x in self.query_field_name_list],
            *[x.text for x in self.select_units]
        )
        # from子句
        stmt = self._set_stmt_join(stmt)
        # where子句
        stmt = stmt.filter(text(f'{self.model.__tablename__}.id = :id'))
        # 执行sql，获取查询结果
        row = self.model.env.db.execute(stmt, params={'id': id}).first()
        if not row:
            return
        # 处理成实例并返回
        record = self._deal_read_result_to_instance(row)
        # 处理对多字段
        await self._deal_to_many_field(record)
        return record

    def get_all_relation_units(self):
        units = []
        for column in self.all_fields:
            if isinstance(column.type, Many2one):
                # Many2one字段
                unit = self.__get_many2one_select_unit(column.name, column)
            elif isinstance(column.type, String) and column.type.translate:
                # 多语言字符串字段
                unit = self.__get_string_translate_select_unit(column.name, column)
            else:
                continue
            units.append(unit)
        return units

    def _parse_select(self):
        """ 解析生成select子句查询列 """
        for column in self.all_fields:
            if column.name not in self.field_name_list:
                continue
            if isinstance(column.type, Many2one):
                # Many2one字段
                unit = self.__get_many2one_select_unit(column.name, column)
            elif isinstance(column.type, String) and column.type.translate:
                # 多语言字符串字段
                unit = self.__get_string_translate_select_unit(column.name, column)
            else:
                continue
            self.select_units.append(unit)
        self.select_unit_map = {unit.field.name: unit for unit in self.select_units}

    def _parse_where(self):
        """ 解析生成where子句查询条件 """
        if not self.query:
            return
        self.__query_to_clauses(
            self.query_clauses,
            self.query
        )

    def _set_stmt_join(
            self,
            stmt: Select,
            no_select: bool = False
    ) -> Select:
        """
        查询语句添加关联表
        :param stmt: 查询语句
        :return: 查询语句
        """

        def join_clause(_stmt, units):
            for unit in units:
                if unit.key in keys:
                    continue
                for i, from_alias in enumerate(unit.from_alias):
                    _stmt = _stmt.outerjoin(from_alias, unit.join_on[i])
                keys.append(unit.key)
            return _stmt

        keys = []
        if not no_select:
            stmt = join_clause(stmt, self.select_units)
        stmt = join_clause(stmt, self.where_units)
        return stmt

    def _deal_search_read_order(
            self,
            stmt: Select,
            order: List[List[str]] | List[Tuple[str, str]]
    ) -> Select:
        """
        处理search_read方法的排序规则
        :param stmt: 查询语句
        :param order: 排序规则列表
        """
        if not order:
            return stmt
        o_list = []
        for o in order:
            select_unit = self.select_unit_map.get(o[0])
            order_field = select_unit.text if select_unit else getattr(self.model.__class__, o[0])
            if o[1].lower() == 'desc':
                o_list.append(desc(order_field))
            else:
                o_list.append(asc(order_field))
        return stmt.order_by(*o_list)

    @staticmethod
    def _deal_search_read_page(
            stmt: Select,
            offset: int,
            limit: int,
            no_page: bool
    ) -> Select:
        """
        处理search_read方法分页逻辑
        :param stmt: 查询语句
        :param offset: 从第多少条开始，不包含这个数字
        :param limit: 查询多少条
        :param no_page: 是否不分页
        """
        if no_page:
            return stmt
        return stmt.offset(offset).limit(limit)

    async def _deal_to_many_field(self, records: List[ModelClass] | ModelClass):
        """ 对多字段单独处理 """
        if not isinstance(records, list):
            records = [records]
        for column in self.field_type_dict.get(Many2many.__name__, []):
            await self.__deal_many_to_many_field(column, records)
        for column in self.field_type_dict.get(One2many.__name__, []):
            await self.__deal_one_to_many_field(column, records)

    def _deal_read_result_to_instance(
            self,
            row: list
    ) -> ModelClass:
        """
        处理单条查询结果为实例对象
        :param row: 单条的查询结果
        """
        instance = self.model.__class__()
        for i, field in enumerate(self.query_field_name_list):
            setattr(
                instance,
                field,
                row[i]
            )
        relation_idx_start = len(self.query_field_name_list)
        for i in range(relation_idx_start, len(row)):
            relation_unit = self.select_units[i - relation_idx_start]
            if isinstance(relation_unit.field.type, Many2one):
                # Many2one字段处理成 tuple(id, name)
                m2o_id = getattr(instance, relation_unit.field.name)
                if not m2o_id:
                    continue
                setattr(
                    instance,
                    relation_unit.field.name,
                    (m2o_id, row[i])
                )
            elif isinstance(relation_unit.field.type, String) and relation_unit.field.type.translate:
                # 多语言字符串字段用关联多语言表的查询结果覆盖原字段值
                setattr(
                    instance,
                    relation_unit.field.name,
                    row[i]
                )
        return instance

    def __get_many2one_select_unit(
            self,
            key: str,
            column: Column
    ) -> SelectRelationUnit:
        """
        获取Many2one字段的select子句关联信息单元
        :param key: 唯一标识，用于辨别关联表时是否已关联，避免重复关联
        :param column: 主表的关联字段
        """
        if not isinstance(column.type, Many2one):
            raise ValidationError(f'column [{column.name}] type is not Many2one')
        many2one_model_alias_name = self.__get_alias_name(f'{key}_{column.type.model}')
        many2one_model = ModelContainer.get_obj(column.type.model)
        many2one_text_field = 'name' if hasattr(many2one_model.__class__, 'name') else 'id'
        many2one_text_column = getattr(many2one_model.__class__, many2one_text_field)

        self.alias_name_list.append(many2one_model_alias_name)

        field_text = text(f'{many2one_model_alias_name}.{many2one_text_field}')
        from_alias = [alias(many2one_model.__class__, many2one_model_alias_name)]
        join_on = [text(f'{many2one_model_alias_name}.id') == column]

        if isinstance(many2one_text_column.type, String) and many2one_text_column.type.translate:
            # 关联字段是多语言字段，则需关联多语言表查询
            translate_relation_unit = self.__get_string_translate_select_unit(
                key=f'{key}_translate',
                column=many2one_text_column,
                main_model=many2one_model,
                main_alias_name=many2one_model_alias_name
            )
            field_text = translate_relation_unit.text
            from_alias.extend(translate_relation_unit.from_alias)
            join_on.extend(translate_relation_unit.join_on)
        return SelectRelationUnit(key, column, field_text, from_alias, join_on, RelationFieldType.MANY2ONE)

    def __get_string_translate_select_unit(
            self,
            key: str,
            column: Column,
            main_model: ModelClass = None,
            main_alias_name: str = None
    ) -> SelectRelationUnit:
        """
        获取String translate字段的select子句关联信息单元
        :param key: 唯一标识，用于辨别关联表时是否已关联，避免重复关联
        :param column: 主表的关联字段
        :param main_model: 主表的Model
        :param main_alias_name: 主表在查询中的别名
        :return: SelectRelationUnit
        """
        translate_model_alias_name = self.__get_alias_name(f'{column.name}_translate')
        translate_model = ModelContainer.get('translate')
        translation_field_text = f"{translate_model_alias_name}.translate ->> '{self.model.env.context.language}'"

        self.alias_name_list.append(translate_model_alias_name)

        main_table_name = main_model.__tablename__ if main_model else self.model.__tablename__
        main_model_name = main_model.get_model_name() if main_model else self.model.get_model_name()

        if main_alias_name:
            origin_field_text = f'{main_alias_name}.{column.name}'
            join_on_first = text(f'{translate_model_alias_name}.record_id = {main_alias_name}.id')
        else:
            origin_field_text = f'{main_table_name}.{column.name}'
            join_on_first = text(f'{translate_model_alias_name}.record_id = {main_table_name}.id')

        field_text = text(f"COALESCE({translation_field_text}, {origin_field_text})")
        from_alias = [alias(translate_model, translate_model_alias_name)]
        join_on = [
            and_(join_on_first,
                 text(f"{translate_model_alias_name}.model = '{main_model_name}'"),
                 text(f"{translate_model_alias_name}.field = '{column.name}'"))
        ]
        return SelectRelationUnit(key, column, field_text, from_alias, join_on, RelationFieldType.STRING_TRANSLATE)

    def __get_alias_name(
            self,
            fixed_content: str
    ) -> str:
        """
        获取别名
        :param fixed_content: 固定内容
        """
        index = 1
        alias_name = f'{fixed_content}_{index}'
        while alias_name in self.alias_name_list:
            index += 1
            alias_name = f'{fixed_content}_{index}'
        return alias_name

    def __query_to_clauses(
            self,
            query_clauses: List[ColumnElement[bool]],
            query: Dict[str, Any]
    ):
        """
        递归处理查询条件，把传进来的dict处理成SqlAlchemy查询条件列表
        :param query_clauses: SqlAlchemy查询条件列表容器
        :param query: 原始查询条件
        """
        rel = query.get('rel')
        if not rel:
            condition = query.get('condition')
            query_clauses.append(self.__query_condition_to_clause(condition))
        else:
            rel_func = QUERY_REL_FUNC_MAP.get(rel)
            conditions = query.get('conditions')
            conditions_clauses = []
            for condition in conditions:
                self.__query_to_clauses(conditions_clauses, condition)
            query_clauses.append(rel_func(*conditions_clauses))

    def __query_condition_to_clause(
            self,
            condition: List | Tuple[str, str, Any]
    ) -> ColumnElement[bool] | TextClause:
        """
        处理单个查询条件，把列表格式的查询条件处理成单个的SqlAlchemy查询条件
        :param condition: 原始查询参数的单个查询条件
        """
        select_unit = self.select_unit_map.get(condition[0])
        if select_unit and select_unit.rel_type == RelationFieldType.STRING_TRANSLATE:
            param_name = self.__get_alias_name(f'{select_unit.field.name}_param')
            self.params[param_name] = condition[2]
            condition_text_clause = self.__get_condition_text_clause(condition, select_unit.text, param_name)
            self.where_units.append(WhereRelationUnit(
                key=select_unit.key,
                field=select_unit.field,
                query_clause=condition_text_clause,
                from_alias=select_unit.from_alias,
                join_on=select_unit.join_on
            ))
            return condition_text_clause
        return self.__get_condition_column_element(condition)

    def __get_condition_column_element(
            self,
            condition: List | Tuple[str, str, Any]
    ) -> ColumnElement[bool]:
        """
        获取ColumnElement[bool]类型的查询条件
        :param condition: 原始查询参数的单个查询条件
        """
        column = getattr(self.model.__class__, condition[0])
        compare_symbol = condition[1]
        compare_value = condition[2]
        if compare_symbol == CompareSymbol.EQUAL.value:
            return column == compare_value
        elif compare_symbol == CompareSymbol.NOT_EQUAL.value:
            return column != compare_value
        elif compare_symbol == CompareSymbol.GT.value:
            return column > compare_value
        elif compare_symbol == CompareSymbol.LT.value:
            return column < compare_value
        elif compare_symbol == CompareSymbol.GTE.value:
            return column >= compare_value
        elif compare_symbol == CompareSymbol.LTE.value:
            return column <= compare_value
        elif compare_symbol == CompareSymbol.LIKE.value:
            return column.like(f'%{compare_value}%')
        elif compare_symbol == CompareSymbol.ILIKE.value:
            return column.ilike(f'%{compare_value}%')
        elif compare_symbol == CompareSymbol.IN.value:
            return column.in_(compare_value)
        else:
            raise ValidationError(f'the compare symbol {compare_symbol} is not supported!')

    def __get_condition_text_clause(
            self,
            condition: List | Tuple[str, str, Any],
            select_text: TextClause,
            param_name: str
    ) -> TextClause:
        """
        获取TextClause类型的查询条件
        :param condition: 原始查询参数的单个查询条件
        :param select_text: 对应select子句中的查询语句
        :param param_name: 查询语句的参数名称
        """
        compare_symbol = condition[1]
        compare_value = condition[2]
        text_clause = text(f"{select_text} {compare_symbol} :{param_name}")
        if compare_symbol in (CompareSymbol.LIKE.value, CompareSymbol.ILIKE.value):
            self.params[param_name] = f'%{compare_value}%'
        elif compare_symbol == CompareSymbol.IN.value:
            self.params[param_name] = tuple(compare_value)
        return text_clause

    async def __deal_many_to_many_field(self, column: Column, records: List[ModelClass]):
        """ 查询Many2many字段的值，并写入到原查询结果 """
        assert isinstance(column.type, Many2many)
        model_cls = ModelContainer.get(column.type.model)
        name_column = model_cls.name if hasattr(model_cls, 'name') else model_cls.id
        table = model_cls.__tablename__
        name_text = f'{table}.{name_column.name}'
        join_translate = ''
        if isinstance(name_column.type, String) and name_column.type.translate:
            translate_field_text = f"t_translate.translate ->> '{self.model.env.context.language}'"
            name_text = f'COALESCE({translate_field_text}, {table}.{name_column.name})'
            join_translate = f"""LEFT JOIN t_translate ON (t_translate.model = '{model_cls.get_model_name()}'
                                                   AND t_translate.record_id = {table}.id
                                                   AND t_translate.field = '{name_column.name}')"""
        m2m_rel_table_name = column.type.table_name
        left_field_name = column.type.field_name
        right_field_name = column.type.rel_field_name
        sql = f"""SELECT {table}.id, {name_text}, {m2m_rel_table_name}.{left_field_name}
                    FROM {table} {join_translate}
                         JOIN {m2m_rel_table_name} ON {m2m_rel_table_name}.{right_field_name} = {table}.id
                   WHERE {m2m_rel_table_name}.{left_field_name} IN :ids"""

        origin_record_map = {x.id: x for x in records}
        m2m_rows = self.model.env.db.execute(text(sql), params={'ids': tuple(list(origin_record_map.keys()))}).all()
        if not m2m_rows:
            return
        m2m_rows.sort(key=lambda x: x[2])
        for origin_record_id, m2m_row_group in groupby(m2m_rows, key=lambda x: x[2]):
            origin_record = origin_record_map.get(origin_record_id)
            if not origin_record:
                continue
            origin_record.set(column.name, [(x[0], x[1]) for x in list(m2m_row_group)])

    async def __deal_one_to_many_field(self, column: Column, records: List[ModelClass]):
        assert isinstance(column.type, One2many)
        origin_record_map = {x.id: x for x in records}
        rel_ids = list(origin_record_map.keys())
        rel_field_name = column.type.rel_field_name
        fields = self.field_dict.get(column.name)
        data = await self.model.env[column.type.model].search_read(
            fields=fields,
            query={'rel': 'and', 'conditions': [{'condition': (rel_field_name, 'in', rel_ids)}]},
            no_page=True
        )
        for rel_field_value, rec in groupby(data, key=lambda x: getattr(x, rel_field_name)):
            origin_record = origin_record_map.get(rel_field_value[0])
            if not origin_record:
                continue
            origin_record.set(column.name, [await x.to_dict(fields) for x in rec])

    @staticmethod
    def __get_field_name_list_and_dict(fields: List[str | dict]):
        field_name_list = []
        field_dict = {}
        for field in fields:
            if isinstance(field, str):
                field_name_list.append(field)
                field_dict[field] = field
            elif isinstance(field, dict):
                field, child_fields = list(field.items())[0]
                field_name_list.append(field)
                field_dict[field] = child_fields
        if 'id' not in field_name_list:
            field_name_list.append('id')
            field_dict['id'] = 'id'
        return field_name_list, field_dict

    def __get_query_field_name_list(self):
        query_fields = []
        many2many_fields = [x.name for x in self.field_type_dict.get(Many2many.__name__, [])]
        one2many_fields = [x.name for x in self.field_type_dict.get(One2many.__name__, [])]
        for field in self.field_name_list:
            if field in many2many_fields or field in one2many_fields:
                continue
            query_fields.append(field)
        return query_fields

    def __get_field_type_dict(self):
        type_map = {}
        for f in self.field_name_list:
            if not hasattr(self.model.__class__, f):
                raise ValidationError(f'model [{self.model.get_model_name()}] do not have field [{f}]')
            field: Column = getattr(self.model.__class__, f)
            field_type_name = field.type.__class__.__name__
            if type_map.get(field_type_name):
                type_map[field_type_name].append(field)
            else:
                type_map[field_type_name] = [field]
        return type_map


class WriteHelper:
    def __init__(
            self,
            model: ModelClass,
            values: dict = None
    ):
        self.model = model
        self.values = values
        self.all_fields_map = {x.name: x for x in self.model.__class__.__table__.columns}

        self.many2many_fields = []
        self.one2many_fields = []
        self.translate_string_fields = []

    async def create_help(self):
        """ 创建数据 """
        values = self._filter_values()
        stmt = insert(self.model.__class__).values(values)
        res = self.model.env.db.execute(stmt)
        self.model.set('id', res.returned_defaults[0])
        await self._on_create()

    async def update_help(self):
        """ 更新数据 """
        values = self._filter_values()
        await self._on_update()
        stmt = (
            update(self.model.__class__)
            .filter_by(id=self.model.id)
            .values(values)
        )
        self.model.env.db.execute(stmt)

    async def delete_help(self, ids: List[int] = None):
        """ 删除数据 """
        ids = ids if ids else [self.model.id]
        if not ids:
            raise ValidationError('please specify the record you want to delete')
        self._get_special_fields()
        await self._on_delete(ids)
        stmt = (
            delete(self.model.__class__)
            .filter(text(f'{self.model.__tablename__}.id IN :ids'))
        )
        params = {'ids': tuple(ids)}
        self.model.env.db.execute(stmt, params)

    async def _on_create(self):
        """ 创建数据时，额外处理的逻辑 """
        # 关联翻译记录创建
        for field in self.translate_string_fields:
            await self.model.env['translate'].create({
                'model': self.model.get_model_name(),
                'record_id': self.model.id,
                'field': field.name,
                'translate': {self.model.env.context.language: self.values[field.name]}
            })
        # Many2many字段关联表记录创建
        for field in self.many2many_fields:
            await self.__many2many_record_batch_create(field)
        for field in self.one2many_fields:
            await self.__one2many_record_batch_create(field)

    async def _on_update(self):
        """ 更新数据时，额外处理的逻辑 """
        # 关联翻译记录创建或修改
        for field in self.translate_string_fields:
            translate_obj: ModelClass = self.model.env['translate']
            row: ModelClass = await translate_obj.search_read(
                ['id', 'translate'],
                {'rel': 'and',
                 'conditions': [
                     {'condition': ['model', '=', self.model.get_model_name()]},
                     {'condition': ['record_id', '=', self.model.id]},
                     {'condition': ['field', '=', field.name]},
                 ]}, limit=1)
            if not row:
                await translate_obj.create({
                    'model': self.model.get_model_name(),
                    'record_id': self.model.id,
                    'field': field.name,
                    'translate': {self.model.env.context.language: self.values[field.name]}
                })
            else:
                new_translate_info = row.translate
                new_translate_info[self.model.env.context.language] = self.values[field.name]
                await translate_obj.set('id', row.id).update({'translate': new_translate_info})
        # Many2many字段关联表记录更新
        for field in self.many2many_fields:
            await self.__many2many_record_batch_delete(field)
            await self.__many2many_record_batch_create(field)

    async def _on_delete(self, ids):
        """ 删除数据时，额外处理的逻辑 """
        # 关联翻译删除
        for field in self.translate_string_fields:
            translate_cls = self.model.env['translate'].__class__
            stmt = delete(translate_cls).filter(
                text(f'{translate_cls.__tablename__}.model = :model'),
                translate_cls.record_id.in_(tuple(ids)),
                text(f'{translate_cls.__tablename__}.field = :field')
            )
            self.model.env.db.execute(
                stmt,
                params={
                    'model': self.model.get_model_name(),
                    'field': field.name
                }
            )
        # Many2many字段关联表记录删除
        for field in self.many2many_fields:
            await self.__many2many_record_batch_delete(field, ids)

    def _filter_values(self):
        """ 过滤创建或更新记录的参数 """
        filter_values = {}
        for key, value in self.values.items():
            if key == 'id':
                continue
            if key not in self.all_fields_map:
                raise ValidationError(f'model [{self.model.get_model_name()} do not have filed [{key}]]')

            field = self.all_fields_map.get(key)
            if isinstance(field.type, Many2many):
                self.many2many_fields.append(field)
                continue
            elif isinstance(field.type, One2many):
                self.one2many_fields.append(field)
                continue
            elif isinstance(field.type, String) and field.type.translate:
                self.translate_string_fields.append(field)
            filter_values[key] = value
        return filter_values

    def _get_special_fields(self):
        """ 获取当前model的特殊字段 """
        for field in self.all_fields_map.values():
            if isinstance(field.type, Many2many):
                self.many2many_fields.append(field)
            if isinstance(field.type, One2many):
                self.one2many_fields.append(field)
            elif isinstance(field.type, String) and field.type.translate:
                self.translate_string_fields.append(field)

    async def __many2many_record_batch_create(self, field: Column):
        """ 多对多字段关联表记录批量创建 """
        rel_ids: List[int] = self.values.get(field.name)
        if not rel_ids:
            return
        assert isinstance(field.type, Many2many)
        table_name = field.type.table_name
        field_name = field.type.field_name
        rel_field_name = field.type.rel_field_name
        self.model.env.db.execute(
            text(
                f'''
                     INSERT INTO {table_name}
                     ({field_name}, {rel_field_name})
                     VALUES (:{field_name}, :{rel_field_name})
                '''
            ),
            [
                {
                    field_name: self.model.id,
                    rel_field_name: rel_id
                }
                for rel_id in rel_ids
            ]
        )

    async def __one2many_record_batch_create(self, field: Column):
        """ 一对多字段关联表记录批量创建 """
        values_list: List[dict] = self.values.get(field.name)
        if not values_list:
            return
        assert isinstance(field.type, One2many)
        model = field.type.model
        model_obj = self.model.env[model]
        rel_field_name = field.type.rel_field_name
        for values in values_list:
            values[rel_field_name] = self.model.id
            await model_obj.create(values)

    async def __many2many_record_batch_delete(self, field: Column, ids: List[int] = None):
        """ 多对多字段关联表记录批量删除 """
        ids = ids if ids else [self.model.id]
        if not ids:
            return
        assert isinstance(field.type, Many2many)
        table_name = field.type.table_name
        field_name = field.type.field_name
        self.model.env.db.execute(
            text(
                f'''
                     DELETE FROM {table_name}
                      WHERE {field_name} IN :ids
                '''
            ),
            params={'ids': tuple(ids)}
        )
