from sqlalchemy import (
    TypeDecorator,
    Integer as SqlAlchemyInteger,
    JSON,
    String as SqlalchemyString,
    DateTime as SqlalchemyDateTime,
    Boolean as SqlalchemyBoolean,
)

from framework.exceptions import ValidationError


class Integer(TypeDecorator):
    """整数字段"""
    impl = SqlAlchemyInteger
    cache_ok = True


class String(TypeDecorator):
    """
    字符串字段
    """
    impl = SqlalchemyString
    cache_ok = True

    def __init__(
            self,
            translate: bool = False,
            *args,
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.translate = translate


class Boolean(TypeDecorator):
    """
    布尔字段
    """
    impl = SqlalchemyBoolean
    cache_ok = True


class DateTime(TypeDecorator):
    """
    日期时间字段
    """
    impl = SqlalchemyDateTime
    cache_ok = True


class Many2one(TypeDecorator):
    """
    多对一字段，适用于外键字段，关联其他表的主键
    """
    impl = Integer
    cache_ok = True

    def __init__(self, model: str, ondelete: str = 'cascade', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        self.ondelete = ondelete


class Many2many(TypeDecorator):
    """
    多对多字段
    """
    impl = JSON
    cache_ok = True

    def __init__(
            self,
            model: str,
            table_name: str,
            field_name: str,
            rel_field_name: str,
            *args,
            **kwargs
    ):
        super().__init__(*args, **kwargs)
        self.model = model
        self.table_name = table_name
        self.field_name = field_name
        self.rel_field_name = rel_field_name

    def process_bind_param(self, value, dialect):
        value = value or []
        if not isinstance(value, list):
            raise ValidationError('Many2many field value must be a list')
        if value and not isinstance(value[0], int):
            raise ValidationError('Many2many field value must be a list of int')
        return value


class One2many(TypeDecorator):
    """
    一对多字段
    """
    impl = JSON
    cache_ok = True

    def __init__(self, model: str, rel_field_name: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = model
        self.rel_field_name = rel_field_name
