from framework.constants import ResponseCode


class ModelNotFoundException(Exception):
    """
    model找不到或不存在异常
    """
    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return self.msg


class RecordNotExistException(Exception):
    """
    记录找不到或不存在异常
    """

    def __init__(self, msg=None):
        if msg is None:
            msg = 'record not exist'
        self.msg = msg

    def __str__(self):
        return self.msg


class ValidationError(Exception):
    """
    验证错误
    """
    def __init__(self, msg, code=ResponseCode.ERROR, data=None):
        self.code = code
        self.msg = msg
        self.data = data

    def __str__(self):
        return self.msg
