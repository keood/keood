import datetime

import jwt

from framework.constants import ResponseMsg, Datetime
from framework.config import Config


class JWT:
    algorithm = 'HS256'

    @classmethod
    def generate_token(cls, payload):
        expires_in = Config().get_int('system.jwt_expires_in')
        issued_at = datetime.datetime.now(tz=datetime.timezone.utc)
        expired_at = issued_at + datetime.timedelta(minutes=expires_in)
        token = jwt.encode(
            {'payload': payload, 'exp': expired_at},
            Config().get_string('system.jwt_token_key'),
            algorithm=cls.algorithm
        )
        payload.update({'access_token': token, 'expired_at': expired_at.strftime(Datetime.FORMAT)})
        return token

    @classmethod
    def decode_token(cls, token):
        if not token:
            return False, ResponseMsg.TOKEN_INVALID_ERROR
        try:
            decoded = jwt.decode(
                token,
                Config().get_string('system.jwt_token_key'),
                algorithms=[cls.algorithm]
            )
            return True, decoded['payload']
        except jwt.ExpiredSignatureError:
            return False, ResponseMsg.TOKEN_INVALID_ERROR
        except jwt.InvalidTokenError:
            return False, ResponseMsg.TOKEN_INVALID_ERROR
