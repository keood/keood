def split_point_to_upper_underline(res: str):
    return '_'.join([x.upper() for x in res.split('.')])


def str_to_bool(s: str) -> bool:
    if s.lower() == 'true':
        return True
    elif s.lower() == 'false':
        return False
    raise ValueError('Invalid boolean string')
