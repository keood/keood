import hashlib


class HashCrypt:

    @staticmethod
    def sha256_hash(string, salt):
        """
        SHA256加密
        :param string: 待加密字符串
        :param salt:
        :return:
        """
        string += salt
        sha_signature = hashlib.sha256(string.encode()).hexdigest()
        return sha_signature
