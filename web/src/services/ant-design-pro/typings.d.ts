// @ts-ignore
/* eslint-disable */

declare namespace API {
    type LanguageType = "zh-CN" | "en-US";

    type CurrentUser = {
        name?: string;
        avatar?: string;
        user_id?: string;
        username?: string;
        tz?: string,
        language: LanguageType,
        email?: string;
        signature?: string;
        title?: string;
        group?: string;
        tags?: { key?: string; label?: string }[];
        notifyCount?: number;
        unreadCount?: number;
        country?: string;
        access?: string;
        geographic?: {
            province?: { label?: string; key?: string };
            city?: { label?: string; key?: string };
        };
        address?: string;
        phone?: string;
    };

    type LoginResult = {
        code?: number;
        msg?: string;
        data?: any;
    };

    type PageParams = {
        current?: number;
        pageSize?: number;
    };

    type RuleListItem = {
        key?: number;
        disabled?: boolean;
        href?: string;
        avatar?: string;
        name?: string;
        owner?: string;
        desc?: string;
        callNo?: number;
        status?: number;
        updatedAt?: string;
        createdAt?: string;
        progress?: number;
    };

    type RuleList = {
        data?: RuleListItem[];
        /** 列表的内容总数 */
        total?: number;
        success?: boolean;
    };

    type FakeCaptcha = {
        code?: number;
        status?: string;
    };

    type LoginParams = {
        username?: string;
        password?: string;
    };

    type ErrorResponse = {
        /** 业务约定的错误码 */
        errorCode: string;
        /** 业务上的错误信息 */
        errorMessage?: string;
        /** 业务上的请求是否成功 */
        success?: boolean;
    };

    type NoticeIconList = {
        data?: NoticeIconItem[];
        /** 列表的内容总数 */
        total?: number;
        success?: boolean;
    };

    type NoticeIconItemType = 'notification' | 'message' | 'event';

    type NoticeIconItem = {
        id?: string;
        extra?: string;
        key?: string;
        read?: boolean;
        avatar?: string;
        title?: string;
        status?: string;
        datetime?: string;
        description?: string;
        type?: NoticeIconItemType;
    };

    interface Selection {
        [select: string | number]: string | number
    }

    type GetViewInfoResultDataField = {
        name: string,
        string: string,
        type: string,
        selection?: Selection,
        rel_model?: string,
        translate?: boolean,
    };

    type GetViewInfoResultData = {
        fields: GetViewInfoResultDataField[],
    };

    type GetViewInfoResult = {
        code: number,
        msg: string,
        data: GetViewInfoResultData,
    };

    type ListParams = {
        model: string,
        fields: string[],
        query?: object,
        order?: [string, string][],
        current?: number,
        size?: number,
    };

    type ReadParams = {
        model: string,
        id: number,
        fields: string[]
    };

    type ListResultData = {
        total: number,
        result: object[]
    };

    type ListResult = {
        code: number,
        msg: string,
        data: ListResultData,
    };

    type ReadResult = {
        code: number,
        msg: string,
        data: Record<string, any>,
    };

    type NameSearchResult = {
        code: number,
        msg: string,
        data: RequestOptionsType[]
    };

    type CreateParam = {
        model: string,
        data: Record<string, any>
    };

    type DeleteParam = {
        model: string,
        ids: any,
    };

    type CreateResult = {
        code: number,
        msg: string,
        data: Record<string, any>,
    };

    type DeleteResult = {
        code: number,
        msg: string,
        data?: Record<string, any>,
    };

    type UpdateParam = {
        model: string,
        data: Record<string, any>
    };

    type UpdateResult = {
        code: number,
        msg: string,
        data?: Record<string, any>,
    };
}
