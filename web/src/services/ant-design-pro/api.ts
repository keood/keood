// @ts-ignore
/* eslint-disable */
import {request} from '@umijs/max';
import {history} from "@umijs/max";
import {stringify} from "querystring";

/** 登录接口 POST /api/base/login */
export async function login(body: API.LoginParams) {
    return request<API.LoginResult>('/api/base/login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: body
    });
}

/** 获取当前的用户 GET /api/base/current_user */
export async function currentUser(options?: { [key: string]: any }) {
    return request<{
        data: API.CurrentUser;
    }>('/api/base/current_user', {
        method: 'GET',
        ...(options || {}),
    });
}

/**
 * 退出登录，清楚缓存，保存当前界面
 */
export const loginOut = async () => {
    sessionStorage.removeItem("access_token");
    const {search, pathname} = window.location;
    const urlParams = new URL(window.location.href).searchParams;
    /** 此方法会跳转到 redirect 参数所在的位置 */
    const redirect = urlParams.get('redirect');
    // Note: There may be security issues, please note
    if (window.location.pathname !== '/user/login' && !redirect) {
        history.replace({
            pathname: '/user/login',
            search: stringify({
                redirect: pathname + search,
            }),
        });
    }
};

export async function getUserMenus(options?: { [key: string]: any }) {
    return request<{
        data: any;
    }>('/api/base/menus', {
        method: 'GET',
        ...(options || {}),
    });
}


/** 分页获取列表数据 POST /api/common/list */
export async function list(params: API.ListParams) {
    return request<API.ListResult>('/api/common/list', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: params
    });
}

/** 获取数据详情 POST /api/common/read */
export async function read(params: API.ReadParams) {
    return request<API.ReadResult>('/api/common/read', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: params
    });
}

/** 更新数据 PUT /api/common/update */
export async function update(params: API.UpdateParam) {
    return request<API.UpdateResult>('/api/common/update', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: params
    });
}

/** 新建数据 POST /api/common/create */
export async function create(values: API.CreateParam) {
    return request<API.CreateResult>('/api/common/create', {
        method: 'POST',
        data: {
            method: 'post',
            ...(values || {}),
        },
    });
}

/** 删除数据 DELETE /api/common/delete */
export async function remove(params: API.DeleteParam) {
    return request<API.DeleteResult>('/api/common/delete', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: params
    });
}

export async function nameSearch(model: string, keyWords: string) {
    return request<API.NameSearchResult>('/api/common/name_search', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: {model: model, search_key: keyWords},
    });
}

export async function getViewInfo(model: string, fields: string[]) {
    return request<API.GetViewInfoResult>('/api/base/get_view_info', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        data: {model: model, fields: fields}
    });
}
