﻿import type {RequestOptions} from '@@/plugin-request/request';
import type {RequestConfig} from '@umijs/max';
import {message} from 'antd';
import {history} from "@umijs/max";

// 与后端约定的响应数据格式
interface ResponseStructure {
    code: number;
    msg: string;
    data: any;
}

const authHeaderInterceptor = (url: string, options: RequestConfig) => {
    let accessToken = sessionStorage.getItem('access_token');
    let authHeader = {};
    if (accessToken != null) {
        authHeader = {Authorization: `Bearer ${accessToken}`};
    }
    return {
        url: `${url}`,
        options: {...options, interceptors: true, headers: authHeader},
    };
};

export const errorConfig: RequestConfig = {
    // 错误处理： umi@3 的错误处理方案。
    errorConfig: {
        // 错误抛出
        errorThrower: (res) => {
            const {code, msg, data} = res as unknown as ResponseStructure;
            const error: any = new Error(msg);
            error.info = {code, msg, data};
            switch (code) {
                case 1:
                    error.name = "CommonError";
                    throw error;
                case 401:
                    error.name = "UnauthorizedError";
                    throw error;
            }
        },
        // 错误接收及处理
        errorHandler: (error: any, opts: any) => {
            if (opts?.skipErrorHandler) throw error;
            // 我们的 errorThrower 抛出的错误。
            if (error.name === 'CommonError') {
                message.error(error.info.msg).then();
            } else if (error.name === "UnauthorizedError") {
                message.error(error.info.msg)
                    .then(() => history.push("/user/login"));
            } else if (error.response) {
                // Axios 的错误
                // 请求成功发出且服务器也响应了状态码，但状态代码超出了 2xx 的范围
                message.error(`Response status:${error.response.status}`).then();
            } else if (error.request) {
                // 请求已经成功发起，但没有收到响应
                // \`error.request\` 在浏览器中是 XMLHttpRequest 的实例，
                // 而在node.js中是 http.ClientRequest 的实例
                message.error('None response! Please retry.').then();
            } else {
                // 发送请求时出了点问题
                message.error('Request error, please retry.').then();
            }
        },
    },
    // 请求拦截器
    requestInterceptors: [
        (config: RequestOptions) => {
            return {...config};
        },
        authHeaderInterceptor
    ],

    // 响应拦截器
    responseInterceptors: [
        (response) => {
            const {data} = response as unknown as ResponseStructure;
            // @ts-ignore
            errorConfig.errorConfig.errorThrower(data);
            return response;
        },
    ],
};
