import React, {useContext, useEffect, useState} from "react";
import {getFormViewInfo, LanguageContext, pathReplaceLast} from "@/components/View/service";
import {createIntl, ProForm, ProProvider} from "@ant-design/pro-components";
import {create, read, update} from "@/services/ant-design-pro/api";
import {Button, Card, Form, message} from "antd";
import {useParams} from "react-router";
import {history as umiHistory} from "@umijs/max";
import {formatMessage, getLanguageMap} from "@/locales";

const FormView: React.FC<VIEW.FormProps> = (props) => {
    const [viewInfo, setViewInfo] = useState<VIEW.FormViewInfo>({fieldMap: {}});
    const [state, setState] = useState(props.state);
    const [id, setId] = useState(0);
    const [initialValues, setInitialValues] = useState<Record<string, any>>({});
    const [createForm] = Form.useForm();
    const [updateForm] = Form.useForm();
    const params = useParams();
    const language = useContext(LanguageContext);
    const i18nUtil = createIntl(language, getLanguageMap(language));
    const providerValues = useContext(ProProvider);

    const handleCreate = async (value: Record<string, any>) => {
        const res = await create({model: props.model, data: value});
        if (res.code === 0) {
            message.success(res.msg);
            setId(res.data.id)
            setState("read");
            history.replaceState(
                {path: window.location.pathname},
                '',
                pathReplaceLast(window.location.pathname, res.data.id.toString())
            );
        } else {
            message.error(res.msg);
        }
    };

    const handleRead = async () => {
        if (Object.keys(initialValues).length > 0) {
            return initialValues;
        }
        const params: API.ReadParams = {model: props.model, id: id, fields: props.fields.map(f => f.name)};
        const apiRes = await read(params);
        setInitialValues(apiRes.data);
        return apiRes.data;
    };

    const handleUpdate = async (values: Record<string, any>) => {
        const changedFields = Object.entries(values).filter(([key, value]) => {
            const initialValue = initialValues[key];
            const fieldInfo = viewInfo.fieldMap[key];
            switch (fieldInfo.type) {
                case "Many2one":
                    return value !== initialValue[0];
                case "Many2many":
                    const valueSet = new Set(value);
                    const initialValueSet = new Set((initialValue || []).map((item: any[]) => item[0]));
                    return valueSet.size !== initialValueSet.size
                        || [...valueSet].some(i => !initialValueSet.has(i))
                        || [...initialValueSet].some(i => !valueSet.has(i));
                default:
                    return value !== initialValue;
            }
        });
        if (changedFields.length === 0) {
            setState("read");
            return;
        }
        const changedValues = Object.fromEntries(changedFields);
        changedValues.id = id;
        const res = await update({model: props.model, data: changedValues});
        if (res.code === 0) {
            message.success(res.msg);
            setState("read");
            setInitialValues({});
        } else {
            message.error(res.msg);
        }
    };

    useEffect(() => {
        if (Object.keys(viewInfo.fieldMap).length === 0) {
            getFormViewInfo(props)
                .then((res) => {
                    setViewInfo(res);
                    if (state === undefined && params.id !== undefined && parseInt(params.id) > 0) {
                        setId(parseInt(params.id));
                        setState("read");
                    } else {
                        setState("create");
                    }
                });
        }
    }, [viewInfo, id, state]);

    return (
        <ProProvider.Provider value={{...providerValues, intl: i18nUtil}}>
            {/*create form视图*/}
            {(state === undefined || state === "create") &&
                <>
                    <Card bordered={false} size="small">
                        <Button
                            type="primary"
                            key="create"
                            onClick={() => {
                                createForm.submit();
                            }}
                            style={{marginRight: 8}}
                        >
                            {formatMessage(language, "button.save")}
                        </Button>
                        <Button
                            key="cancelCreate"
                            onClick={() => {
                                umiHistory.push(pathReplaceLast(window.location.pathname));
                            }}
                        >
                            {formatMessage(language, "button.cancel")}
                        </Button>
                    </Card>
                    <Card>
                        <ProForm<Record<string, any>>
                            submitter={false}
                            onFinish={handleCreate}
                            form={createForm}
                        >
                            {props.form(viewInfo)}
                        </ProForm>
                    </Card>
                </>
            }

            {/*update form视图*/}
            {(state === "update" && id > 0) &&
                <>
                    <Card bordered={false} size="small">
                        <Button
                            type="primary"
                            key="update"
                            onClick={() => {
                                updateForm.submit();
                            }}
                            style={{marginRight: 8}}
                        >
                            {formatMessage(language, "button.save")}
                        </Button>
                        <Button
                            key="cancelUpdate"
                            onClick={() => {
                                updateForm.resetFields(props.fields.map(f => f.name));
                                setState("read");
                            }}
                        >
                            {formatMessage(language, "button.cancel")}
                        </Button>
                    </Card>
                    <Card>
                        <ProForm<Record<string, any>>
                            request={handleRead}
                            submitter={false}
                            onFinish={handleUpdate}
                            form={updateForm}
                        >
                            {props.form(viewInfo)}
                        </ProForm>
                    </Card>
                </>
            }

            {/*read form视图*/}
            {(state === "read" && id > 0) &&
                <>
                    <Card bordered={false} size="small">
                        <Button
                            type="primary"
                            key="edit"
                            onClick={() => {
                                setState("update");
                            }}
                            style={{marginRight: 8}}
                        >
                            {formatMessage(language, "button.edit")}
                        </Button>
                    </Card>
                    <Card>
                        <ProForm<Record<string, any>>
                            request={handleRead}
                            submitter={false}
                            disabled={true}
                        >
                            {props.form(viewInfo)}
                        </ProForm>
                    </Card>
                </>
            }
        </ProProvider.Provider>
    );
}

export default FormView;
