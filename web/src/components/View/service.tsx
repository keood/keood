import {ProColumns} from "@ant-design/pro-components";
import {getViewInfo, nameSearch} from "@/services/ant-design-pro/api";
import type {SortOrder} from "antd/lib/table/interface";
import {ProFieldValueType} from "@ant-design/pro-utils/lib";
import {createContext} from "react";
import {Tag} from "antd";
import {c} from "@umijs/utils/compiled/tar";

export const LanguageContext = createContext<API.LanguageType>("zh-CN");

/**
 * 后端SqlAlchemy字段类型 与 前端Antd Pro字段类型 映射常量
 */
const FIELD_VALUE_TYPE_MAP: { [fieldType: string]: ProFieldValueType } = {
    String: "text",
    Integer: "digit",
    DateTime: "dateTime",
    Enum: "select",
    Many2one: "select",
    Many2many: "select"
};

/**
 * 获取tree视图信息
 */
export const getTreeViewInfo = async (props: VIEW.TreeProps, language: string) => {
    const fieldMap = getFieldMap(props.fields);
    const apiViewInfo = await getViewInfo(props.model, props.fields.map(f => f.name))
    const apiFieldMap = getApiFieldMap(apiViewInfo.data.fields);

    let columns: ProColumns[] = [];
    props.fields.forEach((field: VIEW.Field) => {
        const rec = apiFieldMap[field.name]
        let column: ProColumns = {
            title: rec.string,
            dataIndex: rec.name,
            tip: rec.string,
            hideInSearch: field.noSearch,
            hideInTable: field.invisible,
            sorter: field.sorter,
            valueType: FIELD_VALUE_TYPE_MAP[rec.type] || "text",
        };
        if (rec.type === "DateTime") {
            column["hideInSearch"] = true;
        } else if (rec.type === "Enum") {
            column["valueEnum"] = rec.selection;
        } else if (rec.type === "Many2one") {
            column["renderText"] = (text) => text ? text[1] : text;
            column["fieldProps"] = {showSearch: true};
            column["request"] = async (params) => {
                const res = await nameSearch(rec.rel_model || "", params.keyWords);
                return res.data;
            };
        } else if (rec.type === "Many2many") {
            column["renderText"] = (items) => {
                if (items && items.length > 0) {
                    return <>
                        {items.map((item: any[], index: number) =>
                            <Tag key={index}
                                 color="processing"
                                 style={{marginRight: 3}}> {item[1]}
                            </Tag>)
                        }
                    </>
                }
                return [];
            };
            column["fieldProps"] = {mode: "multiple"};
        }
        columns.push(column);
        if (rec.type === "DateTime" && !field.noSearch) {
            let searchColumn: ProColumns = {
                title: rec.string,
                dataIndex: rec.name,
                tip: rec.string,
                hideInTable: true,
                valueType: "dateTimeRange",
            };
            columns.push(searchColumn)
        }
        // 前后端字段属性统一写入到VIEW.Field，前端定义的属性优先级高
        field["type"] = rec.type;
    });

    const result: VIEW.TreeViewInfo = {
        fieldMap: fieldMap,
        columns: columns,
    }
    return result;
};

/**
 * 获取form视图信息
 */
export const getFormViewInfo = async (props: VIEW.FormProps) => {
    const apiViewInfo: API.GetViewInfoResult = await getViewInfo(props.model, props.fields.map(f => f.name));
    const apiFieldMap = getApiFieldMap(apiViewInfo.data.fields);

    props.fields.forEach((field) => {
        const apiField = apiFieldMap[field.name];
        field.type = apiField.type;
        field.string = apiField.string;
        field.selection = apiField.selection;
        field.translate = apiField.translate;
        switch (apiField.type) {
            case "Many2one":
                field.request = async (params: any) => {
                    const res = await nameSearch(apiField.rel_model || "", params.keyWords);
                    return res.data;
                };
                break;
            case "Many2many":
                field.request = async (params: any) => {
                    const res = await nameSearch(apiField.rel_model || "", params.keyWords);
                    return res.data;
                };
                break;
        }
    });
    let result: VIEW.FormViewInfo = {
        fieldMap: getFieldMap(props.fields),
    };
    return result;
}

/**
 * 表格查询参数转成后端接口所需的格式
 */
export const tableParamsToQuery = (params: API.PageParams, fieldsMap: Record<string, VIEW.Field>) => {
    let conditions: object[] = [];
    Object.keys(params).forEach((key) => {
        if (["current", "pageSize"].indexOf(key) < 0) {
            const field = fieldsMap[key];
            // @ts-ignore
            const value = params[key];
            conditions.push({condition: [key, field.searchCompareSymbol || "=", value]})
        }
    });
    if (conditions.length === 0) return {};
    return {rel: "and", conditions: conditions};
};

/**
 * 表格排序规则转成后端接口所需的格式
 */
export const tableSortToOrder = (sort: Record<string, SortOrder>) => {
    let order: [string, string][] = [];
    Object.keys(sort).forEach((key) => {
        const value = sort[key];
        if (value === "descend") {
            order.push([key, "desc"])
        } else {
            order.push([key, "asc"])
        }
    });
    return order;
};

/**
 * 替换path最后一个斜杠后的内容
 */
export const pathReplaceLast = (path: string, replaceStr: string = "") => {
    const lastIndex = path.lastIndexOf('/');
    let newPath = path.substring(0, lastIndex);
    if (replaceStr.length > 0) {
        newPath = newPath + "/" + replaceStr
    }
    return newPath;
}

const getFieldMap = (fields: VIEW.Field[]) => {
    return fields.reduce((a: { [fieldName: string]: VIEW.Field }, f) => {
        a[f.name] = f;
        return a;
    }, {});
}

const getApiFieldMap = (apiFields: API.GetViewInfoResultDataField[]) => {
    return apiFields.reduce((m: {
        [key: string]: API.GetViewInfoResultDataField
    }, f) => {
        m[f.name] = f;
        return m;
    }, {});
}
