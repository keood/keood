import React from 'react';
import {useModel} from "@@/exports";
import {PageContainer} from "@ant-design/pro-components";
import {LanguageContext} from './service';
import {ProjectOutlined, SettingOutlined, HomeOutlined,} from "@ant-design/icons";

const View: React.FC<VIEW.LanguagePageContainerProps> = (props) => {
    const {initialState} = useModel('@@initialState');
    const language = initialState?.currentUser?.language;

    return (
        <LanguageContext.Provider value={language || "zh-CN"}>
            <PageContainer>{props.children}</PageContainer>
        </LanguageContext.Provider>
    );
}

export const IconMap: { [key: string]: React.JSX.Element } = {
    'HomeOutlined': <HomeOutlined/>,
    'ProjectOutlined': <ProjectOutlined/>,
    'SettingOutlined': <SettingOutlined/>,
};

export default View;
