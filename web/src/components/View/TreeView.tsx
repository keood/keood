import React, {useContext, useEffect, useRef, useState} from "react";
import {type ActionType, ProTable, ProProvider, createIntl} from "@ant-design/pro-components";
import {
    getTreeViewInfo, LanguageContext,
    tableParamsToQuery,
    tableSortToOrder
} from "@/components/View/service";
import type {SortOrder} from "antd/lib/table/interface";
import {list, remove} from "@/services/ant-design-pro/api";
import {history as umiHistory} from "@umijs/max";
import {Button, message, Modal} from "antd";
import {ExclamationCircleFilled} from "@ant-design/icons";
import {formatMessage, getLanguageMap} from "@/locales";

const TreeView: React.FC<VIEW.TreeProps> = (props) => {
    const actionRef = useRef<ActionType>();
    const rowKey = props.rowKey || "id";
    const [viewInfo, setViewInfo] = useState<VIEW.TreeViewInfo>({});
    const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
    const language = useContext(LanguageContext);
    const i18nUtil = createIntl(language, getLanguageMap(language));
    const providerValues = useContext(ProProvider);

    const handleList = async (
        params: API.PageParams,
        sort: Record<string, SortOrder>,
        filter: Record<string, (string | number)[] | null>
    ) => {
        let listParams: API.ListParams = {
            model: props.model,
            fields: props.fields.map(f => f.name),
            query: tableParamsToQuery(params, viewInfo.fieldMap || {}),
            order: tableSortToOrder(sort),
            current: params.current,
            size: params.pageSize
        }
        const res = await list(listParams);
        return {
            data: res.data.result,
            success: true,
            total: res.data.total,
        };
    };

    const handleDelete = async () => {
        const res = await remove({model: props.model, ids: selectedRowKeys});
        if (res.code === 0) {
            message.success(formatMessage(language, res.msg));
            setSelectedRowKeys([]);
            actionRef.current?.reload();
        } else {
            message.error(formatMessage(language, res.msg));
        }
    };

    const showDeleteConfirm = () => {
        Modal.confirm({
            title: formatMessage(language, "view.table.delete.title"),
            icon: <ExclamationCircleFilled/>,
            okText: formatMessage(language, "view.table.delete.ok"),
            okType: 'danger',
            okButtonProps: {type: "primary"},
            cancelText: formatMessage(language, "view.table.delete.cancel"),
            onOk() {
                handleDelete().then();
            }
        });
    };

    useEffect(() => {
        if (Object.keys(viewInfo).length === 0) {
            getTreeViewInfo(props, language)
                .then((res) => {
                    setViewInfo(res);
                });
        }
    }, [viewInfo]);

    return (
        <ProProvider.Provider value={{...providerValues, intl: i18nUtil}}>
            <ProTable<any, API.PageParams>
                actionRef={actionRef}
                rowKey={rowKey}
                search={{
                    labelWidth: 100
                }}
                toolBarRender={() => {
                    return [
                        <span style={{marginLeft: 8}}>
                        {selectedRowKeys.length > 0 ? `${formatMessage(language, "view.table.selected")} ${selectedRowKeys.length} ${formatMessage(language, "view.table.items")}` : ''}
                    </span>,
                    ];
                }}
                request={handleList}
                columns={viewInfo.columns}
                onRow={(rec) => {
                    return {
                        onDoubleClick: (event) => {
                            umiHistory.push(window.location.pathname + "/" + rec.id.toString());
                        },
                    };
                }}
                rowSelection={{
                    selectedRowKeys: selectedRowKeys,
                    onChange: (selectedRowKeys) => {
                        setSelectedRowKeys(selectedRowKeys);
                    },
                }}
                tableAlertRender={false}
                scroll={{y: 480}}
                toolbar={{
                    search: (
                        <>
                            {!props.options?.noCreate &&
                                <Button
                                    type="primary"
                                    key="new"
                                    onClick={() => {
                                        umiHistory.push(window.location.pathname + "/create");
                                    }}
                                    style={{marginRight: 8}}
                                >
                                    {formatMessage(language, "button.new")}
                                </Button>
                            }
                            {(!props.options?.noDelete && selectedRowKeys.length > 0) &&
                                <Button
                                    type="primary" danger={true}
                                    key="treeDelete"
                                    onClick={() => {
                                        showDeleteConfirm();
                                    }}
                                >
                                    {formatMessage(language, "button.delete")}
                                </Button>
                            }
                        </>
                    ),
                }}
            />
        </ProProvider.Provider>
    );
}

export default TreeView;
