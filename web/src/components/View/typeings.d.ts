declare namespace VIEW {
    type LanguagePageContainerProps = {
        children?: React.ReactNode;
    };

    type FieldAttrs = {
        invisible?: string,
        required?: string,
    };

    type Field = {
        name: string,
        type?: string,
        string?: string,
        invisible?: boolean,
        required?: boolean,
        attrs?: FieldAttrs,
        domain?: string,
        noSearch?: boolean,
        searchCompareSymbol?: string,
        sorter?: boolean,
        selection?: { [select: string | number]: string | number },
        request?: ProFieldRequestData,
        translate?: boolean,
    };

    type Options = {
        noCreate?: boolean,
        noUpdate?: boolean,
        noDelete?: boolean,
        noRead?: boolean,
    };

    type TreeViewInfo = {
        fieldMap?: Record<string, Field>,
        columns?: ProColumns[],
    };

    type FormViewInfo = {
        fieldMap: Record<string, Field>,
    };

    type TreeProps = {
        model: string,
        rowKey?: string,
        options?: Options,
        fields: Field[],
    };

    type FormProps = {
        model: string,
        fields: Field[],
        form: (viewInfo: FormViewInfo) => React.JSX.Element,
        state?: string,
    };

    type FieldProps = {
        field?: Field,
    };
}
