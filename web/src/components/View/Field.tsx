import React from "react";
import {
    ProFormDatePicker,
    ProFormDateTimePicker,
    ProFormDigit,
    ProFormSelect,
    ProFormText
} from "@ant-design/pro-components";
import {GlobalOutlined} from "@ant-design/icons";

const Field: React.FC<VIEW.FieldProps> = ({field}) => {
    const width = "md";
    switch (field?.type) {
        case "String":
            if (field?.translate) {
                return <ProFormText name={field?.name} label={field?.string} width={width}
                                    fieldProps={{addonAfter: <GlobalOutlined/>}}/>;
            } else {
                return <ProFormText name={field?.name} label={field?.string} width={width}/>;
            }
        case "Integer":
            return <ProFormDigit name={field?.name} label={field?.string} width={width}/>;
        case "Date":
            return <ProFormDatePicker name={field?.name} label={field?.string} width={width}/>;
        case "DateTime":
            return <ProFormDateTimePicker name={field?.name} label={field?.string} width={width}/>;
        case "Enum":
            return <ProFormSelect name={field?.name} label={field?.string} valueEnum={field?.selection} width={width}/>
        case "Many2one":
            return <ProFormSelect name={field?.name} label={field?.string} request={field?.request} width={width}
                                  showSearch={true}
                                  transform={(value) => {
                                      const obj: { [key: string]: number } = {};
                                      if (value in Array) {
                                          obj[field?.name] = value[0];
                                      } else {
                                          obj[field?.name] = value[0];
                                      }
                                      return obj;
                                  }}/>
        case "Many2many":
            return <ProFormSelect name={field?.name} label={field?.string} request={field?.request} width={width}
                                  showSearch={true} mode="multiple"
                                  convertValue={(value) => {
                                      if (value && value.length > 0) {
                                          if (value[0] instanceof Array && value.length > 0) {
                                              return value.map((item: any[]) => item[0]);
                                          }
                                      }
                                      return value;
                                  }}
                                  transform={(value) => {
                                      const obj: { [key: string]: number } = {};
                                      if (value && value.length > 0) {
                                          if (value[0] instanceof Array && value[0].length > 0) {
                                              obj[field?.name] = value.map((item: any[]) => item[0]);
                                          } else {
                                              obj[field?.name] = value;
                                          }
                                      } else {
                                          obj[field?.name] = value;
                                      }
                                      return obj;
                                  }}/>
        default:
            return <ProFormText name={field?.name} width={width}/>;
    }
}

export default Field;
