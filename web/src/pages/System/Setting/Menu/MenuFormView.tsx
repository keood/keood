import React from "react";
import {ProForm} from "@ant-design/pro-components";
import FormView from "@/components/View/FormView";
import Field from "@/components/View/Field";
import View from "@/components/View";

const MenuFormView: React.FC = () => {
    const fields: VIEW.Field[] = [
        {name: "name"},
        {name: "path"},
        {name: "icon"},
        {name: "parent_id"},
        {name: "sequence"},
        {name: "created_by"},
        {name: "updated_by"},
        {name: "created_at"},
        {name: "updated_at"},
    ];

    const getForm = (viewInfo: VIEW.FormViewInfo) => {
        return (<>
            <ProForm.Group>
                <Field field={viewInfo.fieldMap["name"]}/>
                <Field field={viewInfo.fieldMap["path"]}/>
                <Field field={viewInfo.fieldMap["icon"]}/>
                <Field field={viewInfo.fieldMap["parent_id"]}/>
                <Field field={viewInfo.fieldMap["sequence"]}/>
            </ProForm.Group>
            <ProForm.Group>
                <Field field={viewInfo.fieldMap["created_by"]}/>
                <Field field={viewInfo.fieldMap["updated_by"]}/>
                <Field field={viewInfo.fieldMap["created_at"]}/>
                <Field field={viewInfo.fieldMap["updated_at"]}/>
            </ProForm.Group>
        </>);
    };

    return (
        <View>
            <FormView
                model="menu"
                fields={fields}
                form={getForm}
            />
        </View>
    );
};

export default MenuFormView;
