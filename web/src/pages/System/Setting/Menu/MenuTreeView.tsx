import React from "react";
import TreeView from "@/components/View/TreeView";
import View from "@/components/View";

const MenuTreeView: React.FC = () => {
  const fields: VIEW.Field[] = [
    {
      name: "id",
      invisible: true,
      noSearch: true,
    },
    {
      name: "name",
      searchCompareSymbol: "like",
    },
    {
      name: "path",
      searchCompareSymbol: "like",
    },
    {
      name: "icon",
      searchCompareSymbol: "like"
    },
    {
      name: "parent_id",
      searchCompareSymbol: "="
    },
  ]
  return (
    <View>
      <TreeView
        model="menu"
        fields={fields}
        options={{noCreate: true, noDelete: true}}
      />
    </View>
  );
};

export default MenuTreeView;
