import React from "react";
import TreeView from "@/components/View/TreeView";
import View from "@/components/View";

const UserTreeView: React.FC = () => {
    const fields: VIEW.Field[] = [
        {
            name: "id",
            invisible: true,
            noSearch: true,
        },
        {
            name: "name",
            searchCompareSymbol: "like",
        },
        {
            name: "username",
            searchCompareSymbol: "like",
        },
        {
            name: "tz",
            searchCompareSymbol: "="
        },
        {
            name: "language",
            searchCompareSymbol: "="
        },
        {
            name: "role_ids",
            noSearch: true
        },
        {
            name: "created_by",
            searchCompareSymbol: "="
        },
        {
            name: "updated_by",
            searchCompareSymbol: "="
        },
        {
            name: "created_at",
            searchCompareSymbol: "=",
            sorter: true,
        },
        {
            name: "updated_at",
            searchCompareSymbol: "=",
            sorter: true,
        }
    ]
    return (
        <View>
            <TreeView
                model="user"
                fields={fields}
            />
        </View>
    );
};

export default UserTreeView;
