import React from "react";
import {ProForm} from "@ant-design/pro-components";
import FormView from "@/components/View/FormView";
import Field from "@/components/View/Field";
import View from "@/components/View";

const UserFormView: React.FC = () => {
    const fields: VIEW.Field[] = [
        {name: "name"},
        {name: "username"},
        {name: "tz"},
        {name: "language"},
        {name: "role_ids"},
        {name: "created_by"},
        {name: "updated_by"},
        {name: "created_at"},
        {name: "updated_at"},
    ];

    const getForm = (viewInfo: VIEW.FormViewInfo) => {
        return (<>
            <ProForm.Group>
                <Field field={viewInfo.fieldMap["name"]}/>
                <Field field={viewInfo.fieldMap["username"]}/>
                <Field field={viewInfo.fieldMap["tz"]}/>
                <Field field={viewInfo.fieldMap["language"]}/>
                <Field field={viewInfo.fieldMap["role_ids"]}/>
                <Field field={viewInfo.fieldMap["created_by"]}/>
                <Field field={viewInfo.fieldMap["updated_by"]}/>
                <Field field={viewInfo.fieldMap["created_at"]}/>
                <Field field={viewInfo.fieldMap["updated_at"]}/>
            </ProForm.Group>
        </>);
    };

    return (
        <View>
            <FormView
                model="user"
                fields={fields}
                form={getForm}
            />
        </View>
    );
};

export default UserFormView;
