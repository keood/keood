import {Footer} from '@/components';
import {login} from '@/services/ant-design-pro/api';
import {LockOutlined, UserOutlined} from '@ant-design/icons';
import {LoginForm, ProFormText} from '@ant-design/pro-components';
import {Helmet, history, useModel} from '@umijs/max';
import {message} from 'antd';
import {createStyles} from 'antd-style';
import React from 'react';
import {flushSync} from 'react-dom';
import Settings from '../../../../config/defaultSettings';
import {formatMessage} from "@/locales";

const useStyles = createStyles(({token}) => {
    return {
        action: {
            marginLeft: '8px',
            color: 'rgba(0, 0, 0, 0.2)',
            fontSize: '24px',
            verticalAlign: 'middle',
            cursor: 'pointer',
            transition: 'color 0.3s',
            '&:hover': {
                color: token.colorPrimaryActive,
            },
        },
        lang: {
            width: 42,
            height: 42,
            lineHeight: '42px',
            position: 'fixed',
            right: 16,
            borderRadius: token.borderRadius,
            ':hover': {
                backgroundColor: token.colorBgTextHover,
            },
        },
        container: {
            display: 'flex',
            flexDirection: 'column',
            height: '100vh',
            overflow: 'auto',
            backgroundImage:
                "url('https://mdn.alipayobjects.com/yuyan_qk0oxh/afts/img/V-_oS6r-i7wAAAAAAAAAAAAAFl94AQBr')",
            backgroundSize: '100% 100%',
        },
    };
});
const Login: React.FC = () => {
    const {initialState, setInitialState} = useModel('@@initialState');
    const {styles} = useStyles();
    const fetchUserInfo = async () => {
        const userInfo = await initialState?.fetchUserInfo?.();
        if (userInfo) {
            flushSync(() => {
                setInitialState((s) => ({
                    ...s,
                    currentUser: userInfo,
                }));
                localStorage.setItem("umi_locale", userInfo.language);
            });
        }
    };
    const handleSubmit = async (values: API.LoginParams) => {
        // 登录
        const response = await login({...values});
        message.success(response.msg);
        sessionStorage.setItem('access_token', response.data.access_token);
        await fetchUserInfo();
        const urlParams = new URL(window.location.href).searchParams;
        history.push(urlParams.get('redirect') || '/');
    };
    const language = (localStorage.getItem("umi_locale") || "zh-CN") as API.LanguageType;
    return (
        <div className={styles.container}>
            <Helmet>
                <title>
                    {formatMessage(language, "page.login.login")}- {Settings.title}
                </title>
            </Helmet>
            <div
                style={{
                    flex: '1',
                    padding: '32px 0',
                }}
            >
                <LoginForm
                    contentStyle={{
                        minWidth: 280,
                        maxWidth: '75vw',
                    }}
                    logo={<img alt="logo" src="/logo.svg"/>}
                    title="KEOOD"
                    subTitle={formatMessage(language, "page.login.subtitle")}
                    initialValues={{
                        autoLogin: true,
                    }}
                    onFinish={async (values) => {
                        await handleSubmit(values as API.LoginParams);
                    }}
                    submitter={{
                        searchConfig: {
                            submitText: formatMessage(language, "button.login"),
                        },
                    }}
                >
                    <>
                        <ProFormText
                            name="username"
                            fieldProps={{
                                size: 'large',
                                prefix: <UserOutlined/>,
                            }}
                            placeholder={formatMessage(language, "page.login.username.placeholder")}
                            rules={[
                                {
                                    required: true,
                                    message: formatMessage(language, "page.login.username.required")
                                },
                            ]}
                        />
                        <ProFormText.Password
                            name="password"
                            fieldProps={{
                                size: 'large',
                                prefix: <LockOutlined/>,
                            }}
                            placeholder={formatMessage(language, "page.login.password.placeholder")}
                            rules={[
                                {
                                    required: true,
                                    message: formatMessage(language, "page.login.password.required"),
                                },
                            ]}
                        />
                    </>
                </LoginForm>
            </div>
            <Footer/>
        </div>
    );
};
export default Login;
