import {PageContainer} from '@ant-design/pro-components';
import React from 'react';
const Welcome: React.FC = () => {
  return (
    <PageContainer>
      <h1>欢迎使用KEOOD</h1>
    </PageContainer>
  );
};

export default Welcome;
