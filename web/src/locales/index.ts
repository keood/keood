import zhCN from "@/locales/zh-CN";
import enUS from "@/locales/en-US";
import {InitialStateType} from "@@/plugin-initialState/@@initialState";

const localeMap = {
    "zh-CN": zhCN,
    "en-US": enUS
};

export const getLanguage = (initialState: InitialStateType) => {
    return initialState?.currentUser?.language || "zh-CN";
};

export const getLanguageMap = (language: API.LanguageType) => {
    return localeMap[language];
};

export const formatMessage = (language: API.LanguageType, message: string) => {
    const languageMap: {[key: string]: any} = localeMap[language];
    if (languageMap.hasOwnProperty(message)) {
        return languageMap[message];
    }
    return message;
};
