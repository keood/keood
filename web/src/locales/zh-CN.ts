export default {
    "view.table.selected": "已选择",
    "view.table.items": "项",
    "view.table.delete.title": "确认删除选择的记录？",
    "view.table.delete.ok": "是",
    "view.table.delete.cancel": "否",

    "button.new": "新建",
    "button.delete": "删除",
    "button.save": "保存",
    "button.cancel": "取消",
    "button.edit": "编辑",
    "button.login": "登录",

    "page.logout": "退出登录",
    "page.login.login": "登录",
    "page.login.subtitle": "一站式后台管理系统",
    "page.login.username.placeholder": "请输入用户名",
    "page.login.username.required": "用户名必填",
    "page.login.password.placeholder": "请输入密码",
    "page.login.password.required": "密码必填",


    tableForm: {
        search: '查询',
        reset: '重置',
        submit: '提交',
        collapsed: '展开',
        expand: '收起',
        inputPlaceholder: '请输入',
        selectPlaceholder: '请选择',
    },
    alert: {
        clear: 'Clear',
    },
    tableToolBar: {
        leftPin: '固定在列首',
        rightPin: '固定在列尾',
        noPin: 'Unpinned',
        leftFixedTitle: '固定在左侧',
        rightFixedTitle: '固定在右侧',
        noFixedTitle: '不固定',
        reset: '重置',
        columnDisplay: '列展示',
        columnSetting: '列设置',
        fullScreen: 'Full Screen',
        exitFullScreen: 'Exit Full Screen',
        reload: '刷新',
        density: '密度',
        densityDefault: 'Default',
        densityLarger: '默认',
        densityMiddle: '中等',
        densitySmall: '紧凑',
    },
    pagination: {
        total: {
            range: '第',
            total: '条/总共',
            item: '条'
        }
    }
}
