export default {
    "view.table.selected": "Selected",
    "view.table.items": "items",
    "view.table.delete.title": "Are you sure delete these records?",
    "view.table.delete.ok": "Yes",
    "view.table.delete.cancel": "No",

    "button.new": "New",
    "button.delete": "Delete",
    "button.save": "Save",
    "button.cancel": "Cancel",
    "button.edit": "Edit",
    "button.login": "Login",

    "page.logout": "Logout",
    "page.login.login": "Login",
    "page.login.subtitle": "One stop backend management system",
    "page.login.username.placeholder": "Please enter username",
    "page.login.username.required": "Username is required",
    "page.login.password.placeholder": "Please enter password",
    "page.login.password.required": "Password is required",

    tableForm: {
        search: 'Query',
        reset: 'Reset',
        submit: 'Submit',
        collapsed: 'Expand',
        expand: 'Collapse',
        inputPlaceholder: 'Please enter',
        selectPlaceholder: 'Please select',
    },
    alert: {
        clear: 'Clear',
    },
    tableToolBar: {
        leftPin: 'Pin to left',
        rightPin: 'Pin to right',
        noPin: 'Unpinned',
        leftFixedTitle: 'Fixed the left',
        rightFixedTitle: 'Fixed the right',
        noFixedTitle: 'Not Fixed',
        reset: 'Reset',
        columnDisplay: 'Column Display',
        columnSetting: 'Settings',
        fullScreen: 'Full Screen',
        exitFullScreen: 'Exit Full Screen',
        reload: 'Refresh',
        density: 'Density',
        densityDefault: 'Default',
        densityLarger: 'Larger',
        densityMiddle: 'Middle',
        densitySmall: 'Compact',
    },
    pagination: {
        total: {
            range: ' ',
            total: 'of',
            item: 'items'
        }
    }
}
