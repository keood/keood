export default JSON.parse(`[
    {
        "path": "/user",
        "layout": false,
        "routes": [
            {
                "name": "登录",
                "path": "/user/login",
                "component": "./User/Login"
            }
        ]
    },
    {
        "path": "/index",
        "name": "index",
        "icon": "HomeOutlined",
        "component": "./Welcome",
        "sequence": 1
    },
    {
        "icon": "SettingOutlined",
        "path": "/system",
        "name": "system",
        "sequence": 1000,
        "routes": [
            {
                "path": "/system/account",
                "name": "account",
                "routes": [
                    {
                        "path": "/system/account/user",
                        "name": "user",
                        "component": "./System/Account/User/UserTreeView"
                    },
                    {
                        "name": "新建用户",
                        "path": "/system/account/user/create",
                        "hideInMenu": true,
                        "component": "./System/Account/User/UserFormView",
                        "parentKeys": [
                            "/system/account/user"
                        ]
                    },
                    {
                        "name": "用户",
                        "path": "/system/account/user/:id",
                        "hideInMenu": true,
                        "component": "./System/Account/User/UserFormView",
                        "parentKeys": [
                            "/system/account/user"
                        ]
                    }
                ]
            },
            {
                "path": "/system/setting",
                "name": "setting",
                "icon": "SettingOutlined",
                "routes": [
                    {
                        "path": "/system/setting/menu",
                        "name": "menu",
                        "component": "./System/Setting/Menu/MenuTreeView"
                    },
                    {
                        "path": "/system/setting/menu/create",
                        "hideInMenu": true,
                        "component": "./System/Setting/Menu/MenuFormView",
                        "parentKeys": [
                            "/system/setting/menu"
                        ]
                    },
                    {
                        "path": "/system/setting/menu/:id",
                        "hideInMenu": true,
                        "component": "./System/Setting/Menu/MenuFormView",
                        "parentKeys": [
                            "/system/setting/menu"
                        ]
                    }
                ]
            }
        ]
    },
    {
        "path": "/",
        "redirect": "/index"
    },
    {
        "path": "*",
        "layout": false,
        "component": "./404"
    }
]`);
