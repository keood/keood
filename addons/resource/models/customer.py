from sqlalchemy import Column, Boolean

from framework.fields import String
from framework.models import Model


class Customer(Model):
    __tablename__ = 't_customer'
    __name = 'customer'

    email = Column(String, unique=True, index=True)
    is_active = Column(Boolean, default=True)
