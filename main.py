from contextlib import asynccontextmanager

from fastapi import FastAPI, Request

import setting
from framework import create_super_user, init_menus, load_models
from framework.addons.base.routers import router as base_router
from framework.config import Config
from framework.constants import ResponseCode, ResponseMsg
from framework.db import Database
from framework.routes import router as common_router, http_exception_handler, JsonResponse
from framework.utils.jwt_util import JWT


@asynccontextmanager
async def lifespan(_):
    Config()
    Database()
    load_models()
    await create_super_user()
    await init_menus()
    yield


app = FastAPI(openapi_url=None, lifespan=lifespan)
app.include_router(common_router)
app.include_router(base_router)


async def token_invalid_error(scope, receive, send):
    response = await get_token_invalid_error_response(scope, receive, send)
    await response(scope, receive, send)


async def get_token_invalid_error_response(scope, receive, send):
    return JsonResponse(
        content=JsonResponse.error(
            code=ResponseCode.UNAUTHORIZED, msg=ResponseMsg.TOKEN_INVALID_ERROR
        )
    )


@app.middleware('http')
async def add_middleware(request: Request, call_next):
    # 校验token
    if request.url.path not in setting.PUBLIC_API_PATH:
        authorization = request.headers.get('Authorization')
        if not authorization or not authorization.startswith('Bearer '):
            return token_invalid_error
        result, info = JWT.decode_token(authorization[7:])
        if not result:
            return token_invalid_error
        request.scope['context'] = info
    return await call_next(request)


app.add_exception_handler(Exception, http_exception_handler)


if __name__ == '__main__':
    import uvicorn

    uvicorn.run(
        app='main:app',
        host='0.0.0.0',
        port=8001,
        log_config='logger.ini'
    )
